/*******************************************************************************
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *    Peter Scheffer - Initial API and implementation 
 *    Chris Culnane - Updated comments and added logging
 ******************************************************************************/
package au.gov.vic.vec;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.Environment;

/**
 * Wrapper for the staging configuration file
 * 
 */
public class StagingConfiguration {

  /**
   * Location of the staging configuration file
   */
  private static final String STAGING_CONFIG_FILE      = Environment.getExternalStorageDirectory().getPath()
                                                           + "/vVoteClient/vvwww/stagingconfig.json";

  /**
   * JSONObject to hold the actual config object
   */
  private JSONObject          stagingConfigurationData = null;

  /**
   * Logger
   */
  private static final Logger logger                   = LoggerFactory.getLogger(StagingConfiguration.class);

  /**
   * Converts a string date into a Calendar object
   * 
   * @param iso8601string
   *          String containing an iso8601 formatted date
   * @return Calendar set to the specified date
   * @throws ParseException
   */
  public static Calendar toCalendar(final String iso8601string) throws ParseException {
    Calendar calendar = Calendar.getInstance();
    String s = iso8601string.replace("Z", "+00:00");
    Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.US).parse(s);
    calendar.setTime(date);
    return calendar;
  }

  /**
   * Create a new StagingConfiguration
   */
  public StagingConfiguration() {
    BufferedReader configBis = null;
    try {
      File inputConfigFile = new File(STAGING_CONFIG_FILE);
      logger.info("Reading Staging Config:{}", STAGING_CONFIG_FILE);
      FileReader configFis = new FileReader(inputConfigFile);
      configBis = new BufferedReader(configFis);

      StringBuffer sb = new StringBuffer();
      String line = null;

      // Read File into StringBuffer
      while ((line = configBis.readLine()) != null) {
        sb.append(line);
      }

      this.stagingConfigurationData = new JSONObject(sb.toString());

    }
    catch (IOException e) {
      logger.error("IOException whilst reading config file", e);
    }
    catch (JSONException e) {
      logger.error("JSONException whilst reading config file", e);
    }
    finally {
      try {
        if (configBis != null) {
          configBis.close();
        }
      }
      catch (IOException e) {
        logger.error("IOException whilst closing config file", e);
      }

    }
  }

  /**
   * Returns a copy of the JSONObject containing the configuration data
   * 
   * @return JSONObject containing the config data
   */
  public JSONObject getStagingConfigurationData() {
    return this.stagingConfigurationData;
  }
}