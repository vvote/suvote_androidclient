/*******************************************************************************
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *    Peter Scheffer - Initial API and implementation 
 *    Chris Culnane - Updated comments, added logging, converted into service
 ******************************************************************************/
package au.gov.vic.vec;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.SignatureException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.http.ParseException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vvote.bouncycastle.jce.provider.BouncyCastleProvider;
import org.vvote.bouncycastle.openpgp.PGPCompressedData;
import org.vvote.bouncycastle.openpgp.PGPException;
import org.vvote.bouncycastle.openpgp.PGPObjectFactory;
import org.vvote.bouncycastle.openpgp.PGPPublicKey;
import org.vvote.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.vvote.bouncycastle.openpgp.PGPSignature;
import org.vvote.bouncycastle.openpgp.PGPSignatureList;
import org.vvote.bouncycastle.openpgp.PGPUtil;
import org.vvote.bouncycastle.openpgp.operator.jcajce.JcaPGPContentVerifierBuilderProvider;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;

/**
 * This Android service downloads a zip file containing the ballot draw data for the election, and verifies that the data is valid
 * via PGP signatures. It unzips the contents onto the web server so that its content can be served to the EAV and VPS app.
 * 
 * @author Peter Scheffer
 */
public class DownloadBundleService extends IntentService {

  /**
   * Logger
   */
  private static final Logger logger                        = LoggerFactory.getLogger(DownloadBundleService.class);

  /**
   * Intent type
   */
  public static final String  PROCESS_RESPONSE              = "au.gov.vic.vec.intent.action.PROCESS_RESPONSE";

  /**
   * Intent request
   */
  public static final String  REQUEST_STRING                = "dlBundleRequest";
  /**
   * Intent response type
   */
  public static final String  RESPONSE_STRING               = "dlBundleResponseType";

  /**
   * Intent response message
   */
  public static final String  RESPONSE_MESSAGE              = "dlBundleResponseMsg";
  /**
   * Intent request for last download date
   */
  public static final String  REQUEST_LAST_DOWNLOAD         = "lastDownload";

  /**
   * Intent request for current status of download service
   */
  public static final String  REQUEST_STATUS                = "status";

  /**
   * Intent to schedule next check
   */
  public static final String  REQUEST_SCHEDULE_TASK         = "scheduleTask";

  /**
   * Intent request for the date of the next scheduled check
   */
  public static final String  REQUEST_NEXT_SCHEDULED        = "nextScheduled";

  /**
   * Intent request for a full status report (all data)
   */
  public static final String  REQUEST_ALL_INFO              = "sendAllInfo";

  /**
   * Intent request for running a scheduled task
   */
  public static final String  SCHEDULED                     = "scheduled";

  /**
   * Intent request to force a download check
   */
  public static final String  REQUEST_DOWNLOAD              = "forceDownload";

  /**
   * Intent request for last download result
   */
  private static final String LAST_DOWNLOAD                 = "lastDownload";

  /**
   * Intent request for the last recorded status message
   */
  private static final String LAST_STATUS                   = "lastStatus";

  /**
   * Intent request for the date of the next scheduled check
   */
  private static final String NEXT_SCHEDULE                 = "nextScheduled";

  /**
   * Filename to use for zip file
   */
  private static final String ZIP_FILENAME                  = "vvote_data.zip";

  /**
   * How often a check is scheduled for
   */
  private static final long   UPDATE_INTERVAL               = 600000;

  /**
   * String path to vvwww path
   */
  private static final String VVOTE_PATH                    = Environment.getExternalStorageDirectory().getPath() + "/vvoteclient/";

  /**
   * String path to vvwww path
   */
  private static final String WEB_SERVER_VVOTE_PATH         = VVOTE_PATH + "vvwww/";

  /**
   * String path to bundle storage location
   */
  private static final String WEB_SERVER_BUNDLE_PATH        = WEB_SERVER_VVOTE_PATH + "bundle/";
  /**
   * Maximum size of zip file
   */
  private static final long   DEFAULT_MAXIMUM_ZIP_FILE_SIZE = 26214400;

  /**
   * Utility method to close a stream. This is useful to limit the nesting of close calls in a finally block
   * 
   * @param stream
   *          InputStream to be closed
   */
  private static void closeStream(InputStream stream) {
    if (stream != null) {
      try {
        stream.close();
      }
      catch (IOException e) {
        logger.error("Exception whilst closing stream", e);
      }
    }
  }

  /**
   * Utility method to close a stream. This is useful to limit the nesting of close calls in a finally block
   * 
   * @param stream
   *          InputStream to be closed
   */
  private static void closeStream(OutputStream stream) {
    if (stream != null) {
      try {
        stream.close();
      }
      catch (IOException e) {
        logger.error("Exception whilst closing stream", e);
      }
    }
  }

  /**
   * Variable to hold instance max zip size, the default can be overridden
   */
  private long                                 maxZipSize;

  /**
   * ArrayList to hold list of files in the bundle
   */
  private ArrayList<String>                    bundleFileArray = new ArrayList<String>();

  /**
   * SharedPreferences to record state of download bundle attempts
   */
  private SharedPreferences                    downloadDetails;

  /**
   * PGP Verifier
   */
  private JcaPGPContentVerifierBuilderProvider verifierBuilder = new JcaPGPContentVerifierBuilderProvider()
                                                                   .setProvider(new BouncyCastleProvider());

  /**
   * Constructor for DownloadBundleService
   */
  public DownloadBundleService() {
    super("Bundle Download Service");

  }

  /**
   * Send an intent broadcast with the last download date
   */
  private void broadcastLastDownload() {
    Intent broadcastIntent = new Intent();
    broadcastIntent.setAction(PROCESS_RESPONSE);
    broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
    broadcastIntent.putExtra(RESPONSE_STRING, REQUEST_LAST_DOWNLOAD);
    broadcastIntent.putExtra(RESPONSE_MESSAGE, this.downloadDetails.getString(LAST_DOWNLOAD, "Never"));
    this.sendBroadcast(broadcastIntent);
  }

  /**
   * Send an intent broadcast with the last status
   */
  private void broadcastLastStatus() {
    Intent broadcastIntent = new Intent();
    broadcastIntent.setAction(PROCESS_RESPONSE);
    broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
    broadcastIntent.putExtra(RESPONSE_STRING, REQUEST_STATUS);
    broadcastIntent.putExtra(RESPONSE_MESSAGE, this.downloadDetails.getString(LAST_STATUS, "Never Run"));
    this.sendBroadcast(broadcastIntent);
  }

  /**
   * Send an intent broadcast with the next scheduled run
   */
  private void broadcastNextSchedule() {
    Intent broadcastIntent = new Intent();
    broadcastIntent.setAction(PROCESS_RESPONSE);
    broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
    broadcastIntent.putExtra(RESPONSE_STRING, REQUEST_NEXT_SCHEDULED);
    broadcastIntent.putExtra(RESPONSE_MESSAGE, this.downloadDetails.getString(NEXT_SCHEDULE, "Not Set"));
    this.sendBroadcast(broadcastIntent);
  }

  /**
   * Checks the headers of the zip for both size and freshness to determine whether a download should be initiated.
   * 
   * @param bundleURL
   *          String URL pointing to the bundle
   * @return boolean true if the headers are valid, false if not
   */
  private boolean checkZipHeaders(String bundleURL) {
    boolean isValid = true;

    URL url;
    URLConnection conn;

    try {
      url = new URL(bundleURL);
      conn = url.openConnection();
    }
    catch (IOException e) {
      logger.error("Exception when openning connection to check headers", e);
      return false;
    }

    // Get header metadata for the zip file.
    String fileLengthStr = conn.getHeaderField("Content-Length");
    String lastModifiedStr = conn.getHeaderField("Last-Modified");
    logger.info("Headers: FileLength:{}, LastModified:{}", fileLengthStr, lastModifiedStr);
    if (fileLengthStr == null || lastModifiedStr == null) {
      logger.error("Zip file is empty or missing on the server");
      return false;
    }

    Long fileLength = Long.parseLong(fileLengthStr);

    // If the file length is greater than the maximum size, declare this zip file invalid.
    if (fileLength > this.maxZipSize) {
      isValid = false;
      logger.error("Zip file exceeds the maximum size. It is {}, the maximum is {}", fileLength, this.maxZipSize);
    }
    return isValid;
  }

  /**
   * Utility method to copy a file from one location to another
   * 
   * @param src
   *          source File to copy
   * @param dst
   *          destination File
   * @throws IOException
   */
  private void copy(File src, File dst) throws IOException {
    InputStream in = null;
    OutputStream out = null;
    try {
      in = new FileInputStream(src);
      out = new FileOutputStream(dst);

      // Transfer bytes from in to out
      byte[] buf = new byte[1024];
      int len;
      while ((len = in.read(buf)) > 0) {
        out.write(buf, 0, len);
      }
    }
    finally {
      closeStream(in);
      closeStream(out);
    }

  }

  /**
   * Performs a download check and broadcasts updates
   */
  private void doDownloadPoll() {
    logger.info("Download Poll Called");
    if (this.downloadDetails.getString(LAST_STATUS, "").equals("SUCCESS")) {
      logger.info("Last status was success, will not download again");
    }
    else {
      Intent broadcastIntent = new Intent();
      broadcastIntent.setAction(PROCESS_RESPONSE);
      broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
      broadcastIntent.putExtra(RESPONSE_STRING, "download");
      DownloadResult downRes = this.pollDownload();
      this.downloadDetails.edit().putString(LAST_STATUS, downRes.name()).commit();
      if (downRes == DownloadResult.SUCCESS) {
        this.downloadDetails.edit().putString(NEXT_SCHEDULE, "Downloaded").commit();
      }
      Calendar cal = Calendar.getInstance();
      this.downloadDetails.edit().putString(LAST_DOWNLOAD, DateFormat.getDateTimeInstance().format(cal.getTime())).commit();
      broadcastIntent.putExtra(RESPONSE_MESSAGE, downRes.name());
      this.sendBroadcast(broadcastIntent);
      this.broadcastLastStatus();
      this.broadcastLastDownload();
      this.broadcastNextSchedule();
    }

  }

  /**
   * Download the actual zip file and store it locally
   * 
   * @param bundleURL
   *          String url for the bundle to download
   * @throws IOException
   */
  private void downloadZipFile(String bundleURL) throws IOException {

    InputStream in = null;
    FileOutputStream out = null;
    try {
      logger.info("About to download {}", bundleURL);
      URL url = new URL(bundleURL);
      URLConnection conn = url.openConnection();
      in = conn.getInputStream();
      out = this.openFileOutput(ZIP_FILENAME, Context.MODE_PRIVATE);
      byte[] b = new byte[1024];
      int count;
      int total = 0;
      // Loop through getting data and check we haven't exceed the maximum size
      while ((count = in.read(b)) >= 0 && total < this.maxZipSize) {
        out.write(b, 0, count);
        total = total + count;
      }
      if (count >= 0) {
        logger.error("Zip file downloaded exceeded maximum allowed size");
      }
      out.flush();

    }
    finally {
      if (out != null) {
        out.close();
      }
      if (in != null) {
        in.close();
      }
    }
  }

  @Override
  public void onCreate() {
    super.onCreate();
    this.downloadDetails = this.getSharedPreferences("DOWNLOAD_DETAILS", MODE_PRIVATE);
    if (!this.downloadDetails.contains(NEXT_SCHEDULE)) {
      this.scheduleNextRun();
    }
    else {
      Date d = new Date();
      try {
        if (!this.downloadDetails.getString(NEXT_SCHEDULE, "").equals("Downloaded")
            && DateFormat.getDateTimeInstance().parse(this.downloadDetails.getString(NEXT_SCHEDULE, "")).before(d)) {
          this.scheduleNextRun();
        }
      }
      catch (java.text.ParseException e) {
        logger.warn("Exception whilst parsing stored date. Will schedule a new check", e);
        this.scheduleNextRun();
      }

    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();

  }

  /**
   * Receives intents and processes them accordingly
   */
  @Override
  protected void onHandleIntent(Intent intent) {

    String dataString = intent.getExtras().getString(REQUEST_STRING);
    if (dataString.equalsIgnoreCase(REQUEST_LAST_DOWNLOAD)) {
      this.broadcastLastDownload();
    }
    else if (dataString.equalsIgnoreCase(REQUEST_STATUS)) {
      this.broadcastLastStatus();
    }
    else if (dataString.equalsIgnoreCase(REQUEST_NEXT_SCHEDULED)) {
      this.broadcastNextSchedule();
    }
    else if (dataString.equalsIgnoreCase(REQUEST_ALL_INFO)) {
      this.broadcastLastDownload();
      this.broadcastLastStatus();
      this.broadcastNextSchedule();
    }
    else if (dataString.equalsIgnoreCase(SCHEDULED)) {
      this.scheduleNextRun();
      this.doDownloadPoll();
    }
    else if (dataString.equalsIgnoreCase(REQUEST_DOWNLOAD)) {
      this.doDownloadPoll();
    }

  }

  /**
   * Perform the actual polling
   * 
   * @return DownloadResult that determines if successful, invalid or an error
   */
  public DownloadResult pollDownload() {
    logger.info("Bundle Polling");

    String bundleURL = null;

    try {
      StagingConfiguration stagingConfig = new StagingConfiguration();
      JSONObject stagingConfigData = stagingConfig.getStagingConfigurationData();
      this.maxZipSize = stagingConfigData.optLong("maxBundleSize", DEFAULT_MAXIMUM_ZIP_FILE_SIZE);
      logger.info("Set max zip size to {}", this.maxZipSize);
      JSONObject event = stagingConfigData.getJSONObject("EventEntity");
      String deviceID = String.valueOf(stagingConfigData.getLong("DeviceId"));

      if (!this.downloadDetails.getString(LAST_STATUS, "").equals("SUCCESS")) {
        BufferedInputStream keyBis = null;
        try {
          logger.info("Loading key from {}", event.getString("VVPubKeyPath"));
          keyBis = new BufferedInputStream(new FileInputStream(event.getString("VVPubKeyPath")));
          PGPPublicKeyRingCollection pgpPubRingCollection = new PGPPublicKeyRingCollection(PGPUtil.getDecoderStream(keyBis));

          // The path on the device to vVote's public key for signature verification.
          // publicKeyPath = event.getString("VVPubKeyPath");
          bundleURL = event.getString("FinalBundlePath") + "?DeviceId=" + deviceID;

          logger.info("Will look for bundle at:{}", bundleURL);
          // Before downloading the zip, check the size of the file, and the freshness of the
          // file to see if it has already been downloaded and checked and found to be bad.
          boolean validZip = this.checkZipHeaders(bundleURL);

          if (!validZip) {
            logger.error("Invalid zip file.");
            return DownloadResult.DOWNLOAD_INVALID;
          }
          logger.info("Zip file appears valid, will try to download");

          this.downloadZipFile(bundleURL);

          if (this.unzipBundleAndVerify(pgpPubRingCollection)) {
            logger.info("Sucessfully unzipped bundle");
            return DownloadResult.SUCCESS;
          }
          else {
            logger.info("Bundle Unzip failed");
            return DownloadResult.FAIL;
          }
        }
        catch (IOException e) {
          logger.error("Exception whilst downloading zip file", e);
          return DownloadResult.FAIL;
        }
        catch (SignatureCheckException e) {
          logger.error("Signature check excpetion", e);
          return DownloadResult.DOWNLOAD_INVALID;
        }
        finally {
          closeStream(keyBis);
        }

      }
      else {
        logger.info("Already downloaded");
        return DownloadResult.FAIL;
      }

    }
    catch (ParseException e) {
      logger.error("Error parsing the configuration file", e);
      return DownloadResult.EXCEPTION;

    }
    catch (Exception e) {
      logger.error("Missing or invalid staging configuration file.", e);
      return DownloadResult.EXCEPTION;
    }

  }

  /**
   * Schedules next download check
   */
  private void scheduleNextRun() {
    logger.info("Scheduling next check");
    if (this.downloadDetails.getString(LAST_STATUS, "").equals("SUCCESS")) {
      logger.info("Have already downloaded bundle, will not schedule another check");
      this.downloadDetails.edit().putString(NEXT_SCHEDULE, "Downloaded").commit();

    }
    else {
      Calendar cal = Calendar.getInstance();
      Intent intent = new Intent(this, DownloadBundleService.class);
      intent.putExtra(REQUEST_STRING, SCHEDULED);

      PendingIntent pendingIntent = PendingIntent.getService(this, 53012, intent, PendingIntent.FLAG_UPDATE_CURRENT);
      AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
      Random random = new Random();
      short s = (short) random.nextInt(Short.MAX_VALUE + 1);
      cal.add(Calendar.MILLISECOND, (int) UPDATE_INTERVAL + (s * 10));
      alarm.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
      logger.info("Scheduling next check for{}", DateFormat.getDateTimeInstance().format(cal.getTime()));
      this.downloadDetails.edit().putString(NEXT_SCHEDULE, DateFormat.getDateTimeInstance().format(cal.getTime())).commit();

    }
    this.broadcastNextSchedule();
  }

  /**
   * Unpacks the zipped bundle file to the web server on the SD card in the correct directory structure.
   * 
   * @param inputFile
   *          - the local data file path to the bundle.zip file.
   * @return success flag
   */
  private boolean unpackZip(String inputFile) {
    logger.info("About to unzip {}", inputFile);
    InputStream is = null;
    ZipInputStream zis = null;
    this.bundleFileArray.clear();
    try {
      String filename;
      File outputPath = this.getFilesDir();

      is = new FileInputStream(outputPath.getAbsolutePath() + inputFile);
      zis = new ZipInputStream(new BufferedInputStream(is));
      ZipEntry zipEntry;
      byte[] buffer = new byte[1024];
      int maxZipEntrySize = 1048576; // 1 MB max expanded file size for each entry in the zip.
      int entrySize = 0;
      int count;

      // Create the folder structure required for the bundle files on the web server.
      File bundleDirectory = new File(WEB_SERVER_BUNDLE_PATH);
      boolean ret = bundleDirectory.mkdirs();
      logger.info("Created bundle directory: {}", ret);
      while ((zipEntry = zis.getNextEntry()) != null) {
        try {
          entrySize = 0;
          filename = zipEntry.getName();

          // Construct new directory if required.
          if (zipEntry.isDirectory()) {
            File fmd = new File(WEB_SERVER_BUNDLE_PATH + filename + "/");
            boolean makeDir = fmd.mkdirs();
            logger.info("Creating directory {}:{}", fmd.getAbsolutePath(), makeDir);
            continue;
          }

          this.bundleFileArray.add(WEB_SERVER_BUNDLE_PATH + filename);
          File output = new File(WEB_SERVER_BUNDLE_PATH + filename);
          logger.info("About to unzip: {}", output.getAbsolutePath());

          FileOutputStream fout = null;
          try {
            fout = new FileOutputStream(output);
            while ((count = zis.read(buffer)) != -1) {
              fout.write(buffer, 0, count);
              entrySize += count;
              if (entrySize > maxZipEntrySize) {
                logger.error("Expanded zip entry file size exceeds stated limit for file: {} ", filename);
                return false;
              }
            }
            fout.flush();
          }
          finally {
            fout.close();
          }
        }
        finally {
          zis.closeEntry();
        }
      }
    }
    catch (IOException e) {
      logger.error("IOException whilst unpacking zip", e);

      return false;
    }
    finally {
      if (zis != null) {
        try {
          zis.close();
        }
        catch (IOException e) {
          logger.error("Exception closing zip input stream");
        }
      }
    }

    return true;
  }

  /**
   * Unzips and examines the bundle.zip file by performing PGP checks on the files in it. If it fails, the polling continues. If it
   * succeeds, the polling ends.
   * 
   * @throws SignatureCheckException
   */
  private boolean unzipBundleAndVerify(PGPPublicKeyRingCollection pgpPubRingCollection) throws SignatureCheckException {

    boolean successfullyUnpacked = this.unpackZip("/" + ZIP_FILENAME);

    if (successfullyUnpacked) {
      try {

        // Iterate over the list of unzipped files and verify that each one has an associated .bpg extension.
        Iterator<String> it = this.bundleFileArray.iterator();
        while (it.hasNext()) {

          String inputFileName = it.next();

          // Skip the .bpg (signature) files and find the original content first.
          if (inputFileName.indexOf(".bpg") > 0) {
            continue;
          }

          // Find the associated signature file.
          String signatureFileName = inputFileName + ".bpg";

          boolean validSignature = this.verifyFile(inputFileName, signatureFileName, pgpPubRingCollection);
          if (!validSignature) {
            throw new SignatureCheckException("File failed signature check: " + inputFileName);
          }

        }
        File districtConf = new File(WEB_SERVER_BUNDLE_PATH + "/districtconf.json");
        if (!districtConf.exists()) {
          logger.warn("Bundle does not include districtconf.json");
        }
        else {
          File dest = new File(VVOTE_PATH + "/districtconf.json");

          this.copy(districtConf, dest);
          logger.info("Copied districtconf.json from bundle");
        }
      }
      catch (IOException e) {
        throw new SignatureCheckException("Exception checking signature", e);
      }
      catch (SignatureException e) {
        throw new SignatureCheckException("Exception checking signature", e);
      }
      catch (GeneralSecurityException e) {
        throw new SignatureCheckException("Exception checking signature", e);
      }
      catch (PGPException e) {
        throw new SignatureCheckException("Exception checking signature", e);
      }

      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Verify the specified file is correctly signed
   * 
   * @param fileName
   *          String fileName to check
   * @param signatureFileName
   *          String path to signature file
   * @param keyIn
   *          InputStream that points to key to use for checking
   * @return boolean true if valid, false if not
   * @throws GeneralSecurityException
   * @throws IOException
   * @throws PGPException
   * @throws SignatureException
   * @throws SignatureCheckException
   */
  private boolean verifyFile(String fileName, String signatureFileName, PGPPublicKeyRingCollection pgpPubRingCollection)
      throws GeneralSecurityException, IOException, PGPException, SignatureException, SignatureCheckException {
    InputStream in = null;
    BufferedInputStream signatureFileBis = null;
    BufferedInputStream dIn = null;
    try {
      try {
        signatureFileBis = new BufferedInputStream(new FileInputStream(signatureFileName));
        in = PGPUtil.getDecoderStream(signatureFileBis);
      }
      catch (FileNotFoundException e) {
        logger.error("FileNotException whilst opening signature file", e);
        return false;
      }

      PGPObjectFactory pgpFact = new PGPObjectFactory(in);
      PGPSignatureList p3;

      Object o = pgpFact.nextObject();
      if (o instanceof PGPCompressedData) {
        PGPCompressedData c1 = (PGPCompressedData) o;
        pgpFact = new PGPObjectFactory(c1.getDataStream());
        p3 = (PGPSignatureList) pgpFact.nextObject();
      }
      else {
        p3 = (PGPSignatureList) o;
      }

      // PGPPublicKeyRingCollection pgpPubRingCollection = new PGPPublicKeyRingCollection(PGPUtil.getDecoderStream(keyIn));
      dIn = new BufferedInputStream(new FileInputStream(fileName));
      PGPSignature sig = p3.get(0);
      PGPPublicKey key = pgpPubRingCollection.getPublicKey(sig.getKeyID());
      if (key == null) {
        throw new SignatureCheckException("Public Key is Null - indicates key cannot be found in Ring");
      }
      sig.init(this.verifierBuilder, key);

      int ch;
      while ((ch = dIn.read()) >= 0) {
        sig.update((byte) ch);
      }

      dIn.close();
      signatureFileBis.close();

      if (sig.verify()) {
        return true;
      }
      else {
        return false;
      }
    }
    finally {
      DownloadBundleService.closeStream(in);
      DownloadBundleService.closeStream(signatureFileBis);
      DownloadBundleService.closeStream(dIn);
    }
  }

}
