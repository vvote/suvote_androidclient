/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package au.gov.vic.vec;

/**
 * DownloadResult enum that is used to record the outcome of an attempted bundle download
 * 
 * @author Chris Culnane
 * 
 */
public enum DownloadResult {
  SUCCESS, DOWNLOAD_INVALID, FAIL, EXCEPTION
}
