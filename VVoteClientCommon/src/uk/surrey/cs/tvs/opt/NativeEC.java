/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/

package uk.surrey.cs.tvs.opt;

/**
 * Class that provides access to native optimised ECPoint calculations
 * 
 * @author Chris Culnane
 * 
 */
public class NativeEC {

  // Load library
  static {
    System.loadLibrary("nativeec");
  }

  /**
   * Group exponentiation - in EC it is a multiplication of two points
   * 
   * @param modulus
   * @param a
   * @param b
   * @param x
   * @param y
   * @param exponent
   * @return
   */
  public native static byte[][] exp(byte[] modulus, byte[] a, byte[] b, byte[] x, byte[] y, byte[] exponent);

  /**
   * Group multiplication - in EC it is an add operation
   * 
   * @param modulus
   * @param a
   * @param b
   * @param x1
   * @param y1
   * @param x2
   * @param y2
   * @return
   */
  public native static byte[][] mul(byte[] modulus, byte[] a, byte[] b, byte[] x1, byte[] y1, byte[] x2, byte[] y2);

}
