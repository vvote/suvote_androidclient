/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.android.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.Toast;

/**
 * A hidden activity to receive USB Events and allow the permissions to be saved between runs - this is because Services cannot
 * remember the permissions given to access USB devices
 * 
 * @author Chris Culnane
 * 
 */
public class UsbEventReceiverActivity extends Activity {

  /**
   * Logger
   */
  private static final Logger logger                     = LoggerFactory.getLogger(UsbEventReceiverActivity.class);

  /**
   * Intent action string
   */
  public static final String  ACTION_USB_DEVICE_ATTACHED = "uk.ac.surrey.cs.tvs.vvote.ACTION_USB_DEVICE_ATTACHED";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  /**
   * Normally called by an intent that indicates that a USB device has been connected. It forwards on this notification via a
   * BroadcastIntent.
   */
  @Override
  protected void onResume() {
    super.onResume();

    Intent intent = getIntent();
    if (intent != null) {
      if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
        logger.info("USB Device Connection Detected");
        Parcelable usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

        Toast.makeText(this, "USB Device Connected", Toast.LENGTH_LONG).show();
        // Create a new intent and put the usb device in as an extra
        Intent broadcastIntent = new Intent(ACTION_USB_DEVICE_ATTACHED);
        broadcastIntent.putExtra(UsbManager.EXTRA_DEVICE, usbDevice);

        // Broadcast this event so we can receive it
        sendBroadcast(broadcastIntent);
      }
    }

    // Close the activity
    finish();
  }

}
