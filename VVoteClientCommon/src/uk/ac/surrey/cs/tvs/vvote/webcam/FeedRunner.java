/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.webcam;

import java.io.ByteArrayOutputStream;

import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.vvote.client.AndroidConstants;
import uk.ac.surrey.cs.tvs.vvote.client.VVoteServiceInterface;
import uk.ac.surrey.cs.tvs.vvote.client.webcam.WebCamServiceConnection;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.ford.openxc.webcam.WebcamManager;

/**
 * Thread to connect to the WebCamManager and loop requesting frames. Each frame is converted to a JPEG and base64 encoded, before
 * being sent back to the client via the WebSocket connection
 * 
 * @author Chris Culnane
 * 
 */
public class FeedRunner implements Runnable {

  /**
   * Logger
   */
  private static final Logger     logger  = LoggerFactory.getLogger(FeedRunner.class);
  /**
   * Determines if the feed should be running
   */
  private boolean                 runFeed = false;

  /**
   * Thread to run the FeedRunner on
   */
  private Thread                  feedThread;

  /**
   * WebCamManager that is used to request frames
   */
  private WebcamManager           webCamManager;

  /**
   * Reference to vVoteService this is running on
   */
  private VVoteServiceInterface   service;

  /**
   * Service connection for connecting to the WebCamManager
   */
  private WebCamServiceConnection webCamServiceConnection;

  /**
   * The WebSocket that has requested the feed and where to send the data back to
   */
  private WebSocket               ws;

  /**
   * Start time that we started looking for the barcode
   */
  private long                    startTime;

  /**
   * runtime for looking for a barcode
   */
  private int                     runTime = 10000;

  /**
   * Constructs a new FeedRunner with the respective reference to the service and WebSocket
   * 
   * @param service
   *          VVoteService that has requested this feedRunner
   * @param ws
   *          WebSocket to use for communication
   */
  public FeedRunner(VVoteServiceInterface service, WebSocket ws) {
    this.ws = ws;
    this.service = service;
  }

  /**
   * Loops until stop is called, requesting and sending frames from the WebCamManager. When stop is called the loop is ended and it
   * disconnects from the WebCamManager
   */
  @Override
  public void run() {
    this.startTime = System.currentTimeMillis();
    try {
      while (this.runFeed && (System.currentTimeMillis() < (this.startTime + this.runTime))) {
        try {
          if (this.webCamManager != null && this.webCamManager.isCameraAttached()) {
            Bitmap frame = this.webCamManager.getFrame();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            frame.compress(Bitmap.CompressFormat.JPEG, 90, stream);
            JSONObject resp = new JSONObject();
            resp.put("type", "frame");
            resp.put("content", IOUtils.encodeData(EncodingType.BASE64, stream.toByteArray()));
            this.ws.send(resp.toString());
          }
        }
        catch (JSONException e) {
          logger.warn("JSONException when sending camera frame", e);
        }
      }

    }
    finally {
      if (this.webCamManager != null) {
        this.service.getService().unbindService(this.webCamServiceConnection);
        logger.info("Unbinding from the webcam");
        this.webCamManager = null;
      }
    }
  }

  /**
   * Starts the FeedRunner, which will loop until stop is called. Each loop will request another frame from the WebCamManager and
   * encode it and send it back to the client.
   * 
   * @return boolean true if successfully started, else false
   */
  public boolean start() {
    // Check we aren't already running and that feedThread is null
    if (this.runFeed != true && this.feedThread == null) {
      this.runFeed = true;
      // Connect to the WebCamManager
      Object mServiceSyncToken = new Object();
      this.webCamServiceConnection = new WebCamServiceConnection(mServiceSyncToken);
      this.service.getService().bindService(new Intent(this.service.getService(), WebcamManager.class),
          this.webCamServiceConnection, Context.BIND_AUTO_CREATE);
      try {
        synchronized (mServiceSyncToken) {
          if (this.webCamManager == null) {
            logger.info("WebcamManager is null, will attempt to connect");
            mServiceSyncToken.wait(this.service.getConfig().getIntParameter(AndroidConstants.Config.BARCODE_TIMEOUT));
          }
          this.webCamManager = this.webCamServiceConnection.getWebCamManager();
          if (this.webCamManager == null) {
            logger.warn("Couldn't connect to WebCam");
            try {
              JSONObject resp = new JSONObject();
              resp.put("type", "ERROR");
              resp.put("msg", "Couldn't connect to WebCam");
              this.ws.send(resp.toString());
            }
            catch (JSONException e1) {
              logger.warn("Exception whilst sending error message to client", e1);
            }
            return false;
          }

        }
      }
      catch (InterruptedException e) {
        logger.warn("Interrupted whilst waiting for WebCam Service");
      }
      this.feedThread = new Thread(this);
      this.feedThread.setDaemon(true);
      this.feedThread.start();
      logger.info("Started FeedRunner");
      return true;
    }
    logger.info("feedRunner is already running, stop should be called first");
    return false;

  }

  /**
   * Stops the feedRunner
   */
  public void stop() {
    this.runFeed = false;

  }

}
