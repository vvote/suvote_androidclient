/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client.webcam;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.ford.openxc.webcam.WebcamManager;

/**
 * Manages the connection to the WebCam service, this allows a single instance connected to the native camera with multiple android
 * instances accessing. This improves the robustness and concurrency of accessing a native webcam.
 * 
 * @author Chris Culnane
 * 
 */
public class WebCamServiceConnection implements ServiceConnection {

  /**
   * Service token used for synchronization
   */
  private Object        mServiceSyncToken;
  /**
   * Tag for logcat
   */
  private static String TAG = "WebCamServiceConnection";
  /**
   * WebCam Manager we are connecting to
   */
  private WebcamManager mWebcamManager;

  /**
   * Constructor for WebCamServiceConnection
   * 
   * @param mServiceSyncToken
   *          object to synchronize on
   */
  public WebCamServiceConnection(Object mServiceSyncToken) {
    this.mServiceSyncToken = mServiceSyncToken;
  }

  /**
   * Gets the webcam manager
   * 
   * @return WebcamManager
   */
  public WebcamManager getWebCamManager() {
    return this.mWebcamManager;
  }

  /**
   * Fired when the service is connected, causing retrieval of the WebCamManager
   */
  @Override
  public void onServiceConnected(ComponentName className, IBinder service) {
    Log.i(TAG, "Bound to WebcamManager");
    synchronized (this.mServiceSyncToken) {
      this.mWebcamManager = ((WebcamManager.WebcamBinder) service).getService();
      this.mServiceSyncToken.notify();
    }
  }

  /**
   * Fired when the service disconnected, this shouldn't happen, we should have an orderly disconnect
   */
  @Override
  public void onServiceDisconnected(ComponentName className) {
    Log.w(TAG, "WebcamManager disconnected unexpectedly");
    synchronized (this.mServiceSyncToken) {

      this.mWebcamManager = null;
      this.mServiceSyncToken.notify();
    }
  }
}
