/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

/**
 * Utility class for holding some useful ESCPOS constants
 * 
 * @author Chris Culnane
 * 
 */
public class ESCPOS {

  /**
   * Byte sequence to perform a paper cut
   */
  public static byte[] PAPER_FULL_CUT = new byte[] { 0x1d, 0x56, 0x00 };

  /**
   * Byte sequence to indicate raster data
   */
  public static byte[] S_RASTER_N     = new byte[] { 0x1d, 0x76, 0x30, 0x00 };

  /**
   * Byte sequence to indicate a line feed
   */
  public static byte[] CTL_LF         = new byte[] { 0x0a };
}
