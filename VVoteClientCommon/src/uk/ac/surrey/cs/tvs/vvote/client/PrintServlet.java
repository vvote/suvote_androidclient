/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * PrintServlet provides a simple wrapper for printing to an EpsonPrinter. It receives the content from the client, reformats it and
 * then sends it to the printer.
 * 
 * @author Chris Culnane
 * 
 */
public class PrintServlet implements NanoServlet {

  /**
   * Reference to the EpsonPrinter we will be using
   */
  private EpsonPrinter          ep;

  /**
   * Logger
   */
  private static final Logger   logger              = LoggerFactory.getLogger(PrintServlet.class);

  /**
   * Reference to the overall service config
   */
  private ConfigFile            serviceConfig;

  /**
   * Interface to the service that has called this
   */
  private VVoteServiceInterface service;

  /**
   * Static constant for access image content request
   */
  private static final String   IMAGE_CONTENT_FIELD = "imageContent";

  /**
   * Constructs a new PrintServlet with a reference to an EpsonPrinter object to use
   * 
   * @param ep
   *          EpsonPrinter to use for printing
   * @param config
   *          ConfigFile for the overall service
   */
  public PrintServlet(VVoteServiceInterface service) {
    logger.info("Created new PrintServlet");
    this.service = service;
    this.ep = this.service.getEpsonPrinter();
    this.serviceConfig = this.service.getConfig();
  }

  /**
   * Create a new temp file in the specified DebugDir with the prefix and suffix. This will not auto delete files
   * 
   * @param prefix
   *          String prefix for temp file name
   * @param suffix
   *          String suffix for temp file name
   * @return File that points to a temp file in the debug directory
   * @throws IOException
   */
  private File getTempDebugFile(String prefix, String suffix) throws IOException {
    File debugDir = new File(this.serviceConfig.getStringParameter(AndroidConstants.Config.DEBUG_DIR));
    if (!debugDir.exists()) {

      boolean ret = debugDir.mkdirs();
      logger.debug("Created debug directory: {}", ret);
    }
    return File.createTempFile(prefix, suffix, debugDir);
  }

  /**
   * runServlet is called by the server to run this particular servlet. It contains the necessary parameters and should return a
   * Response object with the appropriate Response
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {

    if (!params.containsKey(IMAGE_CONTENT_FIELD)) {
      logger.warn("Request does not contain imageContent parameter - will reject");
      return new Response(Response.Status.BAD_REQUEST, NanoHTTPD.MIME_PLAINTEXT, "Missing imageContent parameter");
    }
    else if (!this.ep.isPrinterReady()) {
      logger.warn("Printer is not ready");
      logger.info("Trying to reconnect to printer");
      this.ep = this.service.reconnectPrinter();
      int waitCount = 0;
      try {
        while (!this.ep.isPrinterReady() && waitCount < 10000) {
          logger.info("Printer is still not ready, will try again in 1 second");
          Thread.sleep(1000);
          waitCount = waitCount + 1000;
        }
      }
      catch (InterruptedException e) {
        logger.warn("Interrupted whilst waiting for printer reconnect");
      }
      if (!this.ep.isPrinterReady()) {
        logger.info("Printer is still not ready, will return an error");
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Printer is not ready");
      }

    }
    // Gets the image in Base64 format
    String base64Image = params.get(IMAGE_CONTENT_FIELD);

    if (this.serviceConfig.getBooleanParameter(AndroidConstants.Config.PRINT_DEBUG)
        && this.serviceConfig.hasParam(AndroidConstants.Config.DEBUG_DIR)) {
      logger.debug("Printing is in debug mode - will save decoded image data to file");
      try {
        IOUtils.writeStringToFile("data:image/png;base64," + base64Image, this.getTempDebugFile("printDebug", "request.txt")
            .getAbsolutePath());
      }
      catch (IOException e) {
        logger.error("Exception whilst trying to create debug file", e);
      }

    }

    // Decode the base64 to a byte array
    byte[] decodedString = IOUtils.decodeData(EncodingType.BASE64, base64Image);

    // If in debug mode save it to an image file
    if (this.serviceConfig.getBooleanParameter(AndroidConstants.Config.PRINT_DEBUG)
        && this.serviceConfig.hasParam(AndroidConstants.Config.DEBUG_DIR)) {
      logger.debug("Printing is in debug mode - will save decoded image data to file");
      FileOutputStream fos = null;
      try {
        fos = new FileOutputStream(this.getTempDebugFile("printDebug", "image.png"));
        fos.write(decodedString);
      }
      catch (IOException e) {
        logger.error("Exception whilst trying to create debug file", e);
      }
      finally {
        if (fos != null) {
          try {
            fos.close();
          }
          catch (IOException e) {
            logger.error("Exception whilst trying to close debug file", e);
          }
        }
      }

    }

    // Prepare the images - initially just a bitmap
    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    // Prepare a monochrome image for the canvas the same size as the original - we can only print black or white with the thermal
    // printer
    Bitmap bmpMonochrome = Bitmap.createBitmap(decodedByte.getWidth(), decodedByte.getHeight(), Bitmap.Config.ARGB_8888);

    // Prepare a canvas using the monochrome bitmap
    Canvas canvas = new Canvas(bmpMonochrome);
    ColorMatrix ma = new ColorMatrix();
    ma.setSaturation(0);

    // draw the bitmap onto the canvas
    Paint paint = new Paint();
    paint.setColorFilter(new ColorMatrixColorFilter(ma));
    canvas.drawBitmap(decodedByte, 0, 0, paint);

    // clean up the decoded bitmap image
    decodedByte.recycle();
    // Set the decoded image to be the monochrome version of the image
    decodedByte = bmpMonochrome;

    if (this.serviceConfig.hasParam(AndroidConstants.Config.PRINT_BUFFER)) {
      logger.info("Setting buffer");
      this.ep.setBuffer(this.serviceConfig.getIntParameter(AndroidConstants.Config.PRINT_BUFFER));
    }

    try {
      logger.info("Sending image for printing");
      this.ep.printImage(decodedByte);
      // add some blank lines as a buffer at the end
      for (int i = 0; i < 5; i++) {
        this.ep.getOutputStream().write(ESCPOS.CTL_LF);
      }
      this.ep.getOutputStream().write(ESCPOS.PAPER_FULL_CUT);
      this.ep.getOutputStream().flush();
      logger.info("Finished printing");
    }
    catch (IOException e) {
      logger.error("Exception whilst printing", e);
      return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Error printing image" + e.getMessage());
    }
    logger.info("Finished printing");
    return new Response(Response.Status.OK, NanoHTTPD.MIME_PLAINTEXT, "Printed");

  }

}
