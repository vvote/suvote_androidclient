/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import android.app.Service;

/**
 * Provides an interface for the common functions used by both vVoteClient and vVoteTraining.
 * 
 * @author Chris Culnane
 * 
 */
public interface VVoteServiceInterface {

  /**
   * Gets a reference to the underlying ConfigFile object
   * 
   * @return ConfigFile being used by this service
   */
  public ConfigFile getConfig();

  /**
   * Gets a reference to the connected EpsonPrinter
   * 
   * @return EpsonPrinter that it is connected to
   */
  public EpsonPrinter getEpsonPrinter();

  /**
   * Gets a reference to the actual service object - useful for contexts
   * 
   * @return Service that is running
   */
  public Service getService();

  /**
   * Reconnect the printer and return a reference to it
   * 
   * @return EpsonPrinter that has been reconnected to
   */
  public EpsonPrinter reconnectPrinter();

  /**
   * Broadcasts a status update
   */
  public void sendStatus();

  /**
   * Sets the EpsonPrinter to a new reference. This is useful when a change is detected in the underlying OS and needs to be
   * reflected without restarting the service.
   * 
   * @param updatedPrinter
   *          EpsonPrinter to start using
   */
  public void setEpsonPrinter(EpsonPrinter updatedPrinter);
}
