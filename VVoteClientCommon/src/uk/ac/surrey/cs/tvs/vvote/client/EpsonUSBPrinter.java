/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Build;

/**
 * This is a wrapper for the USBPrinter. It extends the BroadcastReceiver object so it can be registered to listen for intent
 * actions. It implements the EpsonThermalPrinter to provide a unified way of handling potentially different printers, although
 * currently we only have one type of printer supported.
 * 
 * @author Chris Culnane
 * 
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
public class EpsonUSBPrinter extends BroadcastReceiver implements EpsonThermalPrinter {

  /**
   * Logger
   */
  private static final Logger         logger                = LoggerFactory.getLogger(EpsonUSBPrinter.class);
  /**
   * Underlying Android USBManager this will be used for interrogating the USB interface
   */
  private final UsbManager            manager;

  /**
   * Marker to recognise intent response
   */
  private static final String         ACTION_USB_PERMISSION = "uk.ac.surrey.cs.tvs.vvote.client.USB_PERMISSION";

  /**
   * Variable to hold reference to actual USB device
   */
  private UsbDevice                   device;

  /**
   * Boolean holding readystate of device
   */
  private boolean                     isReady               = false;

  /**
   * Underlying USB Device connection
   */
  private UsbDeviceConnection         connection            = null;

  /**
   * USBEndpoint variable
   */
  private UsbEndpoint                 endpoint;

  /**
   * Vendor ID for this printer
   */
  private static final int            EPSON_VENDOR_ID       = 1208;
  /**
   * Stream for sending the data to the USB device
   */
  private BulkUSBBufferedOutputStream out;

  /**
   * Constructs a new USBPrinter object using the current content to iterate through the devices looking for an Epson printer.
   * 
   * @param ctx
   *          Context that this being called from
   */
  public EpsonUSBPrinter(Context ctx) {

    boolean foundDevice = false;
    // Gets the USB Manager
    this.manager = (UsbManager) ctx.getSystemService(Context.USB_SERVICE);

    // Gets a list of all teh USB devices
    HashMap<String, UsbDevice> deviceList = this.manager.getDeviceList();

    // Iterate over the devices
    Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

    while (deviceIterator.hasNext()) {
      this.device = deviceIterator.next();
      logger.debug("Found USB device: {}, VendorID:{}, DeviceID:{}", this.device.getDeviceName(), this.device.getVendorId(),
          this.device.getDeviceId());
      if (this.device.getVendorId() == EPSON_VENDOR_ID) {
        logger.info("Found EPSON device, will assume it is a thermal printer, will stop search");
        foundDevice = true;
        break;
      }

    }
    if (foundDevice) {
      logger.info("Found device, will now ask for permission to access it");
      PendingIntent mPermissionIntent = PendingIntent.getBroadcast(ctx, 0, new Intent(ACTION_USB_PERMISSION), 0);
      IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
      ctx.registerReceiver(this, filter);
      if(!this.manager.hasPermission(this.device)){
        logger.info("Do not have permission for this device");
      }
      this.manager.requestPermission(this.device, mPermissionIntent);
      this.isReady = false;
      IntentFilter unplugFilter = new IntentFilter();
      unplugFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
      ctx.registerReceiver(this, unplugFilter);
    }

  }

  @Override
  public void close() throws IOException {
    if (this.out != null) {
      this.out.close();
    }
    if (this.connection != null) {
      this.connection.close();
    }
  }

  /**
   * Gets a reference to the output stream or null if the device is not ready yet
   * 
   * @return BulkUSBBufferredOutputStream outputstream to send data to the printer
   */
  @Override
  public BulkUSBBufferedOutputStream getOutputStream() {
    if (this.isReady) {
      synchronized (this) {
        return this.out;
      }
    }
    else {
      return null;
    }
  }

  /**
   * Checks whether the device is ready
   * 
   * @return boolean true if ready, false if not
   */
  @Override
  public boolean isReady() {
    return this.isReady;
  }

  /**
   * Method that gets called when receiving an intent. We expect this to be called following the finding of an appropriate device
   * above.
   * 
   * @param context
   *          Context this was called from
   * @param intent
   *          Intent that was broadcast
   */
  @Override
  public void onReceive(Context context, Intent intent) {
    String action = intent.getAction();
    // Check it is the USB Permission intent we asked for previously
    if (ACTION_USB_PERMISSION.equals(action)) {
      logger.info("Received USB Permission intent");
      synchronized (this) {
        // Get the reference to the UsbDevice
        UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        // Check whether permission was granted
        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
          logger.info("Extra permission was granted to access the USB device");
          // Claim the device and open a connection to it
          boolean forceClaim = true;
          UsbInterface intf = device.getInterface(0);
          this.endpoint = intf.getEndpoint(0);
          this.connection = this.manager.openDevice(device);
          this.connection.claimInterface(intf, forceClaim);
          this.out = new BulkUSBBufferedOutputStream(this.connection, this.endpoint);
          logger.info("Connected to device and created output stream");
          this.isReady = true;
        }
        else {
          logger.warn("Permission was not granted to access the device, cannot access it");
        }
      }
    }
    else if (action.equalsIgnoreCase(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
      synchronized (this) {
        UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        if (device.getVendorId() == EPSON_VENDOR_ID) {
          try {
            this.close();
          }
          catch (IOException e) {
            logger
                .info("Received exception when closing Printer - this is expected since it appears the printer has been unplugged");
          }
          logger.warn("Epson Printer has been disconnected");
          this.isReady = false;

        }
      }
    }
  }

  /**
   * Sets the buffer size on the output stream
   * 
   * @param bufferSize
   *          int buffer size to use on the output stream
   */
  @Override
  public void setBuffer(int bufferSize) {
    if (this.getOutputStream() != null) {
      this.getOutputStream().setBufferSize(bufferSize);
    }

  }

  @Override
  public void setIsReady(boolean isReadyUpdate) {
    this.isReady = isReadyUpdate;

  }
}
