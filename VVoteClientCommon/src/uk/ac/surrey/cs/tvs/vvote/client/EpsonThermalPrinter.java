/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface for an EpsonThermal Printer. Currently that is the only printer we support, but in the future we may have to support
 * network based printer as well or none-thermal printers
 * 
 * @author Chris Culnane
 * 
 */
public interface EpsonThermalPrinter {

  /**
   * Closes the connection to the printer - this is a final step, once called this instance is dead, a new instance will need to be
   * created to reconnect to the printer
   * 
   * @throws IOException
   */
  public void close() throws IOException;

  /**
   * Gets the outputstream of the underlying printer
   * 
   * @return OutputStream of underlying printer
   */
  public OutputStream getOutputStream();

  /**
   * Checks if the printer is ready for printing
   * 
   * @return boolean true if ready, false if not
   */
  public boolean isReady();

  /**
   * Sets the buffer size of the underlying stream
   * 
   * @param bufferSize
   *          int buffer size to set
   */
  public void setBuffer(int bufferSize);

  /**
   * Sets the ready state of the printer
   * 
   * @param isReadyUpdate
   *          boolean true if ready, false if not
   */
  public void setIsReady(boolean isReadyUpdate);
}
