/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client.webcam;

/**
 * Exception that occurs if the camera is not attached
 * @author Chris Culnane
 * 
 */
public class CameraNotAttachedException extends Exception {

  /**
   * Required for inherited serialisation.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception with {@code null} as its detail message.
   */
  public CameraNotAttachedException() {

  }

  /**
   * Constructs a new exception with the specified detail message.
   * 
   * @param message
   *          the detail message.
   */
  public CameraNotAttachedException(String detailMessage) {
    super(detailMessage);

  }

  /**
   * Constructs a new exception with the specified detail message and cause.
   * <p>
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically incorporated in this exception's detail
   * message.
   * 
   * @param message
   *          the detail message.
   * @param cause
   *          the cause. A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.
   */
  public CameraNotAttachedException(String detailMessage, Throwable throwable) {
    super(detailMessage, throwable);

  }

  /**
   * Constructs a new exception with the specified cause and a detail message.
   * 
   * @param cause
   *          the cause. A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.
   */
  public CameraNotAttachedException(Throwable throwable) {
    super(throwable);

  }

}
