/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.webcam;

import org.java_websocket.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vvote.json.JSONException;
import org.vvote.json.JSONObject;

import uk.ac.surrey.cs.tvs.comms.http.VECWebSocketListener;
import uk.ac.surrey.cs.tvs.vvote.client.VVoteServiceInterface;

/**
 * Listens for incoming commands to control the WebCam video feed. The two types are start and stop, which start and stop sending
 * received frames from the WebCam
 * 
 * @author Chris Culnane
 * 
 */
public class WebCamFeed implements VECWebSocketListener {

  /**
   * Logger
   */
  private static final Logger   logger     = LoggerFactory.getLogger(WebCamFeed.class);

  /**
   * The FeedRunner runs a loop that requests frames from the WebCam
   */
  private FeedRunner            feedRunner = null;

  /**
   * A reference to the underlying service, we need this to connect to the WebCam Manager service
   */
  private VVoteServiceInterface service;

  /**
   * Constructs a new WebCamFeed object using the passed in service.
   * 
   * @param service
   *          VVoteService that is creating the WebCamFeed
   * 
   */
  public WebCamFeed(VVoteServiceInterface service) {
    this.service = service;
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.VECWebSocketListener#processMessage(org.vvote.json.JSONObject,
   * org.java_websocket.WebSocket)
   */
  @Override
  public void processMessage(JSONObject msg, WebSocket ws) {
    try {
      logger.info("Received {}", msg.toString());
      if (msg.getString("type").equals("start")) {
        // This is a request to start streaming
        if (this.feedRunner == null) {
          // No FeedRunner exists, create a new one
          this.feedRunner = new FeedRunner(this.service, ws);
        }
        else {
          // There is already a feedRunner, it could be old or still running. We only allow one, so stop the existing one and create
          // a new one
          this.feedRunner.stop();
          this.feedRunner = new FeedRunner(this.service, ws);
        }
        if (this.feedRunner.start()) {
          logger.info("Started the FeedRunner - Streaming WebCam Frames");
        }
        else {
          JSONObject resp = new JSONObject();
          resp.put("type", "ERROR");
          resp.put("msg", "The attempt to start the WebCam Stream failed");
          ws.send(resp.toString());
          logger.warn("The attempt to start the WebCam Stream failed");
        }
      }
      else if (msg.getString("type").equals("stop")) {
        // if the feedRunner exists stop it
        if (this.feedRunner != null) {
          this.feedRunner.stop();
        }
      }
    }
    catch (JSONException e) {
      e.printStackTrace();
    }

  }

  /**
   * Stops the WebCam feed by stopping the FeedRunner thread
   */
  public void stop() {
    logger.info("Stop called on the WebCamFeed");
    if (this.feedRunner != null) {
      this.feedRunner.stop();
      logger.info("WebCam Feed Stopped");
      this.feedRunner = null;
    }
  }
}
