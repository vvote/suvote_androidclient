/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client.webcam;

import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.graphics.Bitmap;

import com.ford.openxc.webcam.WebcamManager;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

/**
 * WebCam class to provider wrapper around the WebCam - this is taken from an example in SimpleWebCamera
 * https://bitbucket.org/droidperception/simplewebcam. It uses the underlying library from SimpleWebCam with addition of the code to
 * get a barcode.
 * 
 * @author Chris Culnane
 * 
 */
public class WebCam {

  /**
   * Logger
   */
  private static final Logger logger     = LoggerFactory.getLogger(WebCam.class);

  /**
   * image width variable
   */
  static final int            IMG_WIDTH  = 640;

  /**
   * Image height variable
   */
  static final int            IMG_HEIGHT = 480;

  /**
   * Barcode reader from zxing
   */
  private MultiFormatReader   multiFormatReader;

  /**
   * Creates a new instance of the WebCam and prepares the QRCode scanner
   */
  public WebCam() {
    Map<DecodeHintType, Object> hints = new EnumMap<DecodeHintType, Object>(DecodeHintType.class);
    // We are only looking for QRCodes
    Collection<BarcodeFormat> decodeFormats = EnumSet.of(BarcodeFormat.QR_CODE);
    hints.put(DecodeHintType.POSSIBLE_FORMATS, decodeFormats);
    this.multiFormatReader = new MultiFormatReader();
    this.multiFormatReader.setHints(hints);
  }

  /**
   * Attempts to get a barcode within the specified time or returns null.
   * 
   * @param timeout
   *          int milliseconds to find the barcode
   * @return String with barcode contents or null if no QRCode is found
   */
  public String getBarcode(int timeout, WebcamManager mWebcamManager) throws CameraNotAttachedException {

    long endTime = System.currentTimeMillis() + timeout;
    //logger.info("We have until {} to find the barcode", endTime);

    while (System.currentTimeMillis() < endTime) {
      if (!mWebcamManager.isCameraAttached()) {
        throw new CameraNotAttachedException("No WebCam can be found");
      }
      Bitmap bitmapFromCam = mWebcamManager.getFrame();
      if (bitmapFromCam == null) {
        return null;
      }
      Result rawResult = null;
      // Create a new luminance source from a bitmap obtained via getFrame()
      PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(this.handleBitmap(bitmapFromCam), IMG_WIDTH, IMG_HEIGHT, 0, 0,
          IMG_WIDTH, IMG_HEIGHT, false);
      if (source != null) {
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        try {
          // Try to find a barcode
          rawResult = this.multiFormatReader.decodeWithState(bitmap);
        }
        catch (ReaderException re) {

        }
        finally {
          // reset the reader ready for another go
          this.multiFormatReader.reset();
        }
      }

      // Check if we found anything, if we did, return it
      if (rawResult != null) {
        //logger.info("Found a barcode at {}", System.currentTimeMillis());
        // NOTE: DO NOT LOG THE BARCODE CONTENTS - THE CONTENTS COULD BE SECRET
        return rawResult.getText();

      }
    }

    //logger.info("Couldn't find barcode, will return null");
    return null;

  }

  /**
   * Gets the bitmap data from a bitmap image in JPEG format compressed with a 90 quality factor
   * 
   * @param img
   *          Bitmap to convert to JPEG byte array
   * @return byte array containing JPEG bytes of bitmap
   */
  public byte[] getBitmapData(Bitmap img) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    img.compress(Bitmap.CompressFormat.JPEG, 90, stream);
    return stream.toByteArray();
  }

  /**
   * Converts a bitmap object into a byte array
   * 
   * @param image
   *          Bitmap to be converted
   * @return byte array of image data
   */
  public byte[] handleBitmap(Bitmap image) {
    int w = image.getWidth(), h = image.getHeight();
    int[] rgb = new int[w * h];
    byte[] yuv = new byte[w * h];

    image.getPixels(rgb, 0, w, 0, 0, w, h);
    this.populateYUVLuminanceFromRGB(rgb, yuv, w, h);
    return yuv;
  }

  // Inspired in large part by:
  // http://ketai.googlecode.com/svn/trunk/ketai/src/edu/uic/ketai/inputService/KetaiCamera.java
  private void populateYUVLuminanceFromRGB(int[] rgb, byte[] yuv420sp, int width, int height) {
    for (int i = 0; i < width * height; i++) {
      float red = (rgb[i] >> 16) & 0xff;
      float green = (rgb[i] >> 8) & 0xff;
      float blue = (rgb[i]) & 0xff;
      int luminance = (int) ((0.257f * red) + (0.504f * green) + (0.098f * blue) + 16);
      yuv420sp[i] = (byte) (0xff & luminance);
    }
  }

}
