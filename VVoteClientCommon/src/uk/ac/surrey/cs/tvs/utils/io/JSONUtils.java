/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.JSONConstants;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * JSONUtils contains a number of useful static methods for manipulating the JSON used through the vVote system. For example,
 * converting a JSONArray of preferences into a combined string.
 * 
 * This has been customised for Android to handle the different scripting environment needed for the JSON Schema validation.
 * 
 * @author Chris Culnane
 * 
 */
public class JSONUtils {

  /**
   * Empty string constants for replacing quote characters
   */
  private static final String EMPTY_STRING = "";

  /**
   * Logger
   */
  private static final Logger logger       = LoggerFactory.getLogger(JSONUtils.class);

  /**
   * Compiled script of tv4 and utility method to call to start validation
   */
  private static String       tv4Script;

  /**
   * Load a schema from the given path and return a JsonSchema object
   * 
   * @param schemaPath
   *          String path to the JSON Schema file
   * @return String containing the specified schema
   * @throws JSONIOException
   */
  public static String loadSchema(String schemaPath) throws JSONIOException {
    try {
      return IOUtils.readStringFromFile(schemaPath);
    }
    catch (IOException e) {
      throw new JSONIOException(e);
    }
  }

  /**
   * Converts a JSONArray of permutations into a single string of multiple races
   * 
   * @param perms
   *          JSONArray of permutations
   * @return String of combined permutations and races
   * @throws JSONException
   */
  public static String permutationsToString(JSONArray perms) throws JSONException {
    StringBuffer commitPermutation = new StringBuffer();

    for (int i = 0; i < perms.length(); i++) {
      commitPermutation.append(perms.getJSONArray(i).join(MessageFields.PREFERENCE_SEPARATOR));
      commitPermutation.append(MessageFields.RACE_SEPARATOR);
    }

    return commitPermutation.toString();
  }

  /**
   * Converts a JSONArray of preferences for multiple races into an appropriately structured string of combined preferences
   * 
   * @param races
   *          JSONArray of races
   * @return String of combined preferences and races
   * @throws JSONException
   */
  public static String preferencesToString(JSONArray races) throws JSONException {
    StringBuffer vPrefsSB = new StringBuffer();
    String lcATL = "";
    String lcBTL = "";
    String la = "";

    // Extract the data.
    for (int i = 0; i < races.length(); i++) {
      JSONObject race = races.getJSONObject(i);
      if (race.getString(JSONConstants.Vote.RACE_ID).equalsIgnoreCase(JSONConstants.Vote.REGION_RACE_ATL)) {
        lcATL = race.getJSONArray(JSONConstants.Vote.PREFERENCES).join(MessageFields.PREFERENCE_SEPARATOR)
            .replaceAll(MessageFields.QUOTE_CHAR, JSONUtils.EMPTY_STRING);
      }
      else if (race.getString(JSONConstants.Vote.RACE_ID).equalsIgnoreCase(JSONConstants.Vote.REGION_RACE_BTL)) {
        lcBTL = race.getJSONArray(JSONConstants.Vote.PREFERENCES).join(MessageFields.PREFERENCE_SEPARATOR)
            .replaceAll(MessageFields.QUOTE_CHAR, JSONUtils.EMPTY_STRING);
      }
      else if (race.getString(JSONConstants.Vote.RACE_ID).equalsIgnoreCase(JSONConstants.Vote.DISTRICT_RACE)) {
        la = race.getJSONArray(JSONConstants.Vote.PREFERENCES).join(MessageFields.PREFERENCE_SEPARATOR)
            .replaceAll(MessageFields.QUOTE_CHAR, JSONUtils.EMPTY_STRING);
      }
    }

    // Store it in the preferences.
    vPrefsSB.append(la);
    vPrefsSB.append(MessageFields.RACE_SEPARATOR);
    vPrefsSB.append(lcATL);
    vPrefsSB.append(MessageFields.RACE_SEPARATOR);
    vPrefsSB.append(lcBTL);
    vPrefsSB.append(MessageFields.RACE_SEPARATOR);

    return vPrefsSB.toString();
  }

  /**
   * Validates the underlying message against the passed in JSON Schema
   * 
   * The result is true or false, but a full list of errors is logged.
   * 
   * On Android we include the Rhino JavaScript engine to allow us to run the JavaScript. The Android OS does not include the
   * JavaScript engine found on the desktop JVM
   * 
   * @param schema
   *          String containing schema to use for validation
   * @param json
   *          String of JSON to validate
   * @return true if it is valid, false if not
   */
  public static boolean validateSchema(String schema, String json) {
    try {

      if (JSONUtils.tv4Script == null) {
        InputStreamReader isReader = new InputStreamReader(
            JSONUtils.class.getResourceAsStream(JSONConstants.SchemaProcessing.SCHEMA_JS));
        BufferedReader br = null;
        try {
          br = new BufferedReader(isReader);
          String line = null;
          StringBuffer script = new StringBuffer();
          while ((line = br.readLine()) != null) {
            script.append(line);
          }
          tv4Script = script.toString();
          br.close();
        }
        finally {
          if (br != null) {
            br.close();
          }
        }
      }
      Context rhino = Context.enter();

      // Turn off optimisation to make Rhino Android compatible
      rhino.setOptimizationLevel(-1);
      try {
        Scriptable scope = rhino.initStandardObjects();

        scope.put("data", scope, json);
        scope.put("schema", scope, schema);
        // Note the forth argument is 1, which means the JavaScript source has
        // been compressed to only one line using something like YUI
        Object res = rhino.evaluateString(scope, tv4Script, "ScriptAPI", 1, null);

        JSONObject result = new JSONObject(Context.toString(res));

        if (result.getBoolean(JSONConstants.SchemaProcessing.IS_VALID)) {
          return true;
        }
        else {
          logger.warn("Validating JSON Failed: {}" + result.toString());
        }
      }
      finally {
        // We must exit the Rhino VM
        Context.exit();
      }

    }
    catch (IOException e) {
      logger.warn("IOException whilst performing schema validation, possible missing file", e);
    }
    catch (JSONException e) {
      logger.warn("IOException whilst performing schema validation, possible missing file", e);
    }

    return false;
  }
}
