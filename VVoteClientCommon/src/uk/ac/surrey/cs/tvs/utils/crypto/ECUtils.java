/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.vvote.bouncycastle.jce.ECNamedCurveTable;
import org.vvote.bouncycastle.jce.spec.ECParameterSpec;
import org.vvote.bouncycastle.math.ec.ECPoint;
import org.vvote.json.JSONException;
import org.vvote.json.JSONObject;

import uk.surrey.cs.tvs.opt.NativeEC;

/**
 * This is a wrapper for Elliptic Curve crypto operations. It provides a number of utility methods, as well as some crypto
 * operations. This has been modified to work specifically on Android - more precisely it uses native optimisations to avoid the
 * poor performance of the Android BigInteger class.
 * 
 * @author Chris Culnane
 * 
 */
public class ECUtils {

  /**
   * Default curve to use
   */
  private static final String DEFAULT_CURVE = "P-256";

  /**
   * Field name of x coordinate of a point
   */
  public static final String  X_POINT       = "x";

  /**
   * Field name of y coordinate of a point
   */
  public static final String  Y_POINT       = "y";

  /**
   * Field name of g^r (alpha) of a cipher
   */
  public static final String  GR            = "gr";

  /**
   * Field name of my^r (beta) of a cipher
   */
  public static final String  MYR           = "myr";

  /**
   * P value of the curve from NIST specification - NOTE if the curve is changed this will need changing
   */
  public static byte[]        P             = new BigInteger("0ffffffff00000001000000000000000000000000ffffffffffffffffffffffff",
                                                16).toByteArray();

  /**
   * Convert and ECPoint into a JSONObject - coordinates are stored as Hex representations of the underlying BigInteger. Note, this
   * does not use point compression
   * 
   * @param point
   *          ECPoint to convert
   * @return JSONObject with x and y field of point
   * @throws JSONException
   */
  public static JSONObject ecPointToJSON(ECPoint point) throws JSONException {
    JSONObject jpoint = new JSONObject();
    ECPoint pointNorm = point.normalize();
    jpoint.put(ECUtils.X_POINT, pointNorm.getAffineXCoord().toBigInteger().toString(16));
    jpoint.put(ECUtils.Y_POINT, pointNorm.getAffineYCoord().toBigInteger().toString(16));

    return jpoint;
  }

  /**
   * Static value to hold the a value - we cache it for optimisation reasons
   */
  private byte[]          a;

  /**
   * Static value to hold the b value - we cache it for optimisation reasons
   */
  private byte[]          b;

  /**
   * Static value to hold the g x-coordinate - we cache it for optimisation reasons
   */
  private byte[]          gx;

  /**
   * Static value to hold the g y-coordinate - we cache it for optimisation reasons
   */
  private byte[]          gy;

  /**
   * Holds a reference to the EC parameter spec as defined by the curve name
   */
  private ECParameterSpec params;

  /**
   * Construct an ECUtils object using the default curve
   */
  public ECUtils() {
    this(ECUtils.DEFAULT_CURVE);
  }

  /**
   * Construct an ECUtils object using the specified curve and cache the relevant values for the native optimisation calculations
   * NOTE: Whilst this will load relevant values for a different curve, the P value of the curve is hard coded since it does not
   * appear to be accessible via the Curve Parameters
   * 
   * @param curveName
   *          String curveName
   */
  public ECUtils(String curveName) {
    super();
    this.params = ECNamedCurveTable.getParameterSpec(curveName);
    this.a = this.params.getCurve().getA().toBigInteger().toByteArray();
    this.b = this.params.getCurve().getB().toBigInteger().toByteArray();
    ECPoint gNorm = this.params.getG().normalize();
    this.gx = gNorm.getAffineXCoord().toBigInteger().toByteArray();
    this.gy = gNorm.getAffineYCoord().toBigInteger().toByteArray();
  }

  /**
   * Converts a cipher (two ECPoints) into a JSON object. Each point is converted to a JSON representation using ecPointToJSON and
   * stored in a field for the gr and myr parts.
   * 
   * @param points
   *          array of ECPoints representing the [gr,myr] values
   * @return JSONObject containing JSON representations of each point
   * @throws JSONException
   */
  public JSONObject cipherToJSON(ECPoint[] points) throws JSONException {
    JSONObject cipher = new JSONObject();
    cipher.put(ECUtils.GR, ECUtils.ecPointToJSON(points[0]));
    cipher.put(ECUtils.MYR, ECUtils.ecPointToJSON(points[1]));
    return cipher;
  }

  /**
   * Utility method for ECPoint encryption, generates new randomness from a new instance of SecureRandom and then calls encrypt with
   * the randomness value
   * 
   * @param plaintext
   *          ECPoint representing the plaintext
   * @param publicKey
   *          ECPoint of the public key
   * @return ECPoint array of the cipher, first value is g^r, second is my^r
   */
  public ECPoint[] encrypt(ECPoint plaintext, ECPoint publicKey) {
    // Generate some new randomness
    BigInteger randomness = new BigInteger(this.params.getN().bitLength(), new SecureRandom());

    return this.encrypt(plaintext, publicKey, randomness);
  }

  /**
   * Encrypts an ECPoint plaintext using the ECPoint public key passed in and the specified randomness and performs the calculation
   * natively
   * 
   * @param plaintext
   *          ECPoint representing the plaintext
   * @param publicKey
   *          ECPoint of the public key
   * @param randomness
   *          BigInteger randomness to use (r)
   * @return ECPoint array of the cipher, first value is g^r, second is my^r
   */
  public ECPoint[] encrypt(ECPoint plaintext, ECPoint publicKey, BigInteger randomness) {
    ECPoint g = this.getG().normalize();
    BigInteger x = g.getAffineXCoord().toBigInteger();
    BigInteger y = g.getAffineYCoord().toBigInteger();
    byte[][] res = NativeEC.exp(P, this.a, this.b, this.gx, this.gy, randomness.toByteArray());

    ECPoint gr = this.getParams().getCurve().createPoint(new BigInteger(res[0]), new BigInteger(res[1]));
    ECPoint publicKeyNorm = publicKey.normalize();
    x = publicKeyNorm.getAffineXCoord().toBigInteger();
    y = publicKeyNorm.getAffineYCoord().toBigInteger();
    ECPoint plaintextNorm = plaintext.normalize();
    res = NativeEC.exp(P, this.a, this.b, x.toByteArray(), y.toByteArray(), randomness.toByteArray());
    res = NativeEC.mul(P, this.a, this.b, plaintextNorm.getAffineXCoord().toBigInteger().toByteArray(), plaintextNorm
        .getAffineYCoord().toBigInteger().toByteArray(), res[0], res[1]);
    ECPoint myr = this.getParams().getCurve().createPoint(new BigInteger(res[0]), new BigInteger(res[1]));

    return new ECPoint[] { gr, myr };
  }

  /**
   * Utility method to get the G value of the underlying curve
   * 
   * @return ECPoint of G
   */
  public ECPoint getG() {
    return this.params.getG();
  }

  /**
   * Utility method to get the Order Upper Bound (N) of the underlying curve
   * 
   * @return BigInteger of the order of the upper bound (N)
   */
  public BigInteger getOrderUpperBound() {
    return this.params.getN();
  }

  /**
   * Get the underlying parameter spec for this instance of ECUtils
   * 
   * @return ECParameterSpec for the curve being used
   */
  public ECParameterSpec getParams() {
    return this.params;
  }

  /**
   * Generates a random integer of max length n using the SecureRandom instance passed in
   * 
   * @param n
   *          BigInteger representing max bit length of random value
   * @param rand
   *          SecureRandom to generate integer from
   * @return BigInteger random value - guaranteed to not be equal to n
   */
  public BigInteger getRandomInteger(BigInteger n, SecureRandom rand) {
    BigInteger randValue;
    int maxbits = n.bitLength();
    if(maxbits<=1){
      throw new IllegalArgumentException("The number of bits to generate cannot be less than 2");
    }
    do {
      randValue = new BigInteger(maxbits, rand);
    } while (randValue.compareTo(n) >= 0 || randValue.equals(BigInteger.ZERO));

    return randValue;
  }

  /**
   * Gets a random ECPoint from the underlying curve. Calls getRandomValue with a newly instantiated SecureRandom
   * 
   * @return ECPoint representing a random point on the curve
   */
  public ECPoint getRandomValue() {
    return this.getRandomValue(new SecureRandom());
  }

  /**
   * Gets a random ECPoint from the underlying curve. Uses the SecureRandom object to generate a random integer of suitable order
   * and multiplies the G of the underlying curve by that value
   * 
   * @param rand
   *          SecureRandom to generate random integer from
   * @return ECPoint representing a random point on the curve
   */
  public ECPoint getRandomValue(SecureRandom rand) {
    return this.params.getG().multiply(this.getRandomInteger(this.params.getN(), rand));
  }

  /**
   * Converts a JSONObject, containing two JSONObjects for the g^r and my^r, into an ECPoint array. Calls pointFromJSON to convert
   * the JSONObject for each point.
   * 
   * @param cipher
   *          JSONObject of cipher to convert
   * @return Array of ECPoints, the first is g^r, the second is the my^r
   * @throws JSONException
   */
  public ECPoint[] jsonToCipher(JSONObject cipher) throws JSONException {
    ECPoint[] cipherPoint = new ECPoint[2];
    cipherPoint[0] = this.pointFromJSON(cipher.getJSONObject(ECUtils.GR));
    cipherPoint[1] = this.pointFromJSON(cipher.getJSONObject(ECUtils.MYR));

    return cipherPoint;
  }

  /**
   * Convert a JSONObject with an x and y coordinate into an ECPoint on the underlying curve. The x and y should be hex
   * representations of underlying BigInteger values. Note this does not use point compression
   * 
   * @param obj
   *          JSONObject to convert
   * @return ECPoint with x and y value from JSONObject
   * @throws JSONException
   */
  public ECPoint pointFromJSON(JSONObject obj) throws JSONException {
    return this.params.getCurve().createPoint(new BigInteger(obj.getString(ECUtils.X_POINT), 16),
        new BigInteger(obj.getString(ECUtils.Y_POINT), 16));
  }

  /**
   * Re-encrypts an ECPoint cipher pair using the specified ECPoint public key and BigInteger randomness, performs the operation
   * using native ECPoint calculations
   * 
   * @param cipher
   *          ECPoint array, first element is g^r second element is my^r
   * @param pk
   *          ECPoint of the public Key
   * @param rand
   *          BigInteger holding the randomness to use for the re-encryption
   * @return ECPoint array containing the re-encrypted cipher pair
   */
  public ECPoint[] reencrypt(ECPoint[] cipher, ECPoint pk, BigInteger rand) {
    byte[][] res = NativeEC.exp(P, this.a, this.b, this.gx, this.gy, rand.toByteArray());
    ECPoint[] cipherNorm = new ECPoint[2];
    cipherNorm[0] = cipher[0].normalize();
    cipherNorm[1] = cipher[1].normalize();
    res = NativeEC.mul(P, this.a, this.b, cipherNorm[0].getAffineXCoord().toBigInteger().toByteArray(), cipherNorm[0]
        .getAffineYCoord().toBigInteger().toByteArray(), res[0], res[1]);

    ECPoint gr = this.getParams().getCurve().createPoint(new BigInteger(res[0]), new BigInteger(res[1]));

    ECPoint pkNorm = pk.normalize();
    BigInteger x = pkNorm.getAffineXCoord().toBigInteger();
    BigInteger y = pkNorm.getAffineYCoord().toBigInteger();
    res = NativeEC.exp(P, this.a, this.b, x.toByteArray(), y.toByteArray(), rand.toByteArray());
    res = NativeEC.mul(P, this.a, this.b, cipherNorm[1].getAffineXCoord().toBigInteger().toByteArray(), cipherNorm[1]
        .getAffineYCoord().toBigInteger().toByteArray(), res[0], res[1]);
    ECPoint myr = this.getParams().getCurve().createPoint(new BigInteger(res[0]), new BigInteger(res[1]));
    return new ECPoint[] { gr, myr };

  }
}
