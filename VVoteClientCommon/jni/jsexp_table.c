/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include "gmp.h"
#include "ecn.h"

/*
 * Returns the index'th bit of each of the first block_width integers
 * in the array. The least significant bit in the output is the bit
 * extracted from the first integer in the input array.
 */
static int
getbits(mpz_t *op, int index, size_t block_width)
{
  int i;
  int bits = 0;

  for (i = block_width - 1; i >= 0; i--)
    {
      bits <<= 1;
      if (mpz_tstbit(op[i], index))
	{
	  bits |= 1;
	}
    }
  return bits;
}

void
ecn_jsexp_table(mpz_t ropx, mpz_t ropy, mpz_t ropz,
		ecn_jsexp_tab table,
		mpz_t *exponents,
		size_t max_exponent_bitlen)
{
  int i;
  int index;
  int mask;
  size_t bitlen;

  mpz_t t1;           /* Temporary variable for curve operations. */
  mpz_t t2;           /* Temporary variable for curve operations. */
  mpz_t t3;           /* Temporary variable for curve operations. */
  mpz_t t4;           /* Temporary variable for curve operations. */
  mpz_t t5;           /* Temporary variable for curve operations. */
  mpz_t t6;           /* Temporary variable for curve operations. */
  mpz_t t7;           /* Temporary variable for curve operations. */
  mpz_t t8;           /* Temporary variable for curve operations. */
  mpz_t t9;           /* Temporary variable for curve operations. */

  mpz_t zz;           /* Temporary variable for curve operations. */
  mpz_t zzz;          /* Temporary variable for curve operations. */

  mpz_t *exps;

  size_t len = table->len;
  size_t tabs_len = table->tabs_len;
  size_t block_width = table->block_width;
  size_t last_block_width = len - (tabs_len - 1) * block_width;
  mpz_t **tabsx = table->tabsx;
  mpz_t **tabsy = table->tabsy;
  mpz_t **tabsz = table->tabsz;

  mpz_init(t1);
  mpz_init(t2);
  mpz_init(t3);
  mpz_init(t4);
  mpz_init(t5);
  mpz_init(t6);
  mpz_init(t7);
  mpz_init(t8);
  mpz_init(t9);

  mpz_init(zz);
  mpz_init(zzz);

  /* Initialize result variable. */
  mpz_set_si(ropx, 0);
  mpz_set_si(ropy, 1);
  mpz_set_si(ropz, 0);

  /* Execute simultaneous square-and-multiply. */
  for (index = max_exponent_bitlen - 1; index >= 0; index--)
    {

      /* Square ... */
      ecn_jsquare(t1, t2, t3,
		  t4, t5, t6,
		  ropx, ropy, ropz,
		  table->modulus, table->a,
		  ropx, ropy, ropz);

      /* ... and multiply. */
      i = 0;
      exps = exponents;
      while (i < tabs_len)
	{
	  if (i == tabs_len - 1)
	    {
	      mask = getbits(exps, index, last_block_width);
	    }
	  else
	    {
	      mask = getbits(exps, index, block_width);
	    }

	  mpz_set_si(zz, 0);
	  mpz_set_si(zzz, 0);

	  ecn_jmul(t1, t2, t3,
		   t4, t5,
		   t6, t7,
		   t8, t9,
		   ropx, ropy, ropz,
		   table->modulus, table->a,
		   ropx, ropy, ropz,
		   tabsx[i][mask], tabsy[i][mask], tabsz[i][mask],
		   zz, zzz);
	  i++;
	  exps += block_width;
	}
    }

  mpz_clear(t1);
  mpz_clear(t2);
  mpz_clear(t3);
  mpz_clear(t4);
  mpz_clear(t5);
  mpz_clear(t6);
  mpz_clear(t7);
  mpz_clear(t8);
  mpz_clear(t9);

  mpz_clear(zz);
  mpz_clear(zzz);

}
