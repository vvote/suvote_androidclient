/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <gmp.h>
#include "ecn.h"

void
ecn_jsexp_aff(mpz_t ropx, mpz_t ropy,
	      mpz_t *basesx, mpz_t *basesy,
	      mpz_t *exponents,
	      size_t len,
	      mpz_t modulus, mpz_t a, mpz_t b)
{
  int i;

  mpz_t ropz;
  mpz_t *basesz = ecn_array_alloc_init(len);

  mpz_init(ropz);

  for (i = 0; i < len; i++) {
    ecn_affj(basesx[i], basesy[i], basesz[i], modulus);
  }

  ecn_jsexp(ropx, ropy, ropz,
	    basesx, basesy, basesz,
	    exponents,
	    len,
	    modulus, a, b);

  ecn_jaff(ropx, ropy, ropz, modulus);

  mpz_clear(ropz);

  ecn_array_clear_dealloc(basesz, len);
}
