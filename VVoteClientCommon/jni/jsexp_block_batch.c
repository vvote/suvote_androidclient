/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <gmp.h>
#include "ecn.h"

void
ecn_jsexp_block_batch(mpz_t ropx, mpz_t ropy, mpz_t ropz,
		      mpz_t *basesx, mpz_t *basesy, mpz_t *basesz,
		      mpz_t *exponents,
		      size_t len,
		      mpz_t modulus, mpz_t a, mpz_t b,
		      size_t block_width, size_t batch_len,
		      size_t max_exponent_bitlen)
{
  int i;
  ecn_jsexp_tab table;
  mpz_t tmpx;
  mpz_t tmpy;
  mpz_t tmpz;

  mpz_t t1;           /* Temporary variable for curve operations. */
  mpz_t t2;           /* Temporary variable for curve operations. */
  mpz_t t3;           /* Temporary variable for curve operations. */
  mpz_t U1;           /* Temporary variable for curve operations. */
  mpz_t U2;           /* Temporary variable for curve operations. */
  mpz_t S1;           /* Temporary variable for curve operations. */
  mpz_t S2;           /* Temporary variable for curve operations. */
  mpz_t H;            /* Temporary variable for curve operations. */
  mpz_t r;            /* Temporary variable for curve operations. */

  mpz_t zz;           /* Temporary variable for curve operations. */
  mpz_t zzz;          /* Temporary variable for curve operations. */

  mpz_init(tmpx);
  mpz_init(tmpy);
  mpz_init(tmpz);

  mpz_init(t1);
  mpz_init(t2);
  mpz_init(t3);
  mpz_init(U1);
  mpz_init(U2);
  mpz_init(S1);
  mpz_init(S2);
  mpz_init(H);
  mpz_init(r);

  mpz_init(zz);
  mpz_init(zzz);

  if (len < batch_len) {
    batch_len = len;
  }

  ecn_jsexp_init(table, modulus, a, b, batch_len, block_width);


  /* Initialize result to unit element. */
  mpz_set_si(ropx, 0);
  mpz_set_si(ropy, 1);
  mpz_set_si(ropz, 0);

  for (i = 0; i < len; i += batch_len)
    {

      /* Last batch may be slightly shorter. */
      if (len - i < batch_len)
  	{
  	  batch_len = len - i;

  	  ecn_jsexp_clear(table);
  	  ecn_jsexp_init(table, modulus, a, b, batch_len, block_width);
  	}

      /* Perform computation for batch */
      ecn_jsexp_precomp(table, basesx, basesy, basesz);

      /* Compute batch. */
      ecn_jsexp_table(tmpx, tmpy, tmpz, table, exponents, max_exponent_bitlen);

      mpz_set_si(zz, 0);
      mpz_set_si(zzz, 0);

      /* Multiply with result so far. */
      ecn_jmul(t1, t2, t3,
	       U1, U2,
	       S1, S2,
	       H, r,
	       ropx, ropy, ropz,
	       modulus, a,
	       ropx, ropy, ropz,
	       tmpx, tmpy, tmpz,
	       zz, zzz);

      /* Move on to next batch. */
      basesx += batch_len;
      basesy += batch_len;
      basesz += batch_len;
      exponents += batch_len;
    }

  mpz_clear(zz);
  mpz_clear(zzz);

  mpz_clear(t1);
  mpz_clear(t2);
  mpz_clear(t3);
  mpz_clear(U1);
  mpz_clear(U2);
  mpz_clear(S1);
  mpz_clear(S2);
  mpz_clear(H);
  mpz_clear(r);

  mpz_clear(tmpy);
  mpz_clear(tmpx);
  mpz_clear(tmpz);

  ecn_jsexp_clear(table);
}
