/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <gmp.h>
#include "ecn.h"
#include <time.h>

void
print_time()
{
    time_t timer;
    char buffer[25];
    struct tm* tm_info;

    time(&timer);
    tm_info = localtime(&timer);

    strftime(buffer, 25, "%Y:%m:%d%H:%M:%S\n", tm_info);
    fprintf(stderr, "%s", buffer);
}

void
test_square(mpz_t modulus, mpz_t a, mpz_t b, mpz_t gx, mpz_t gy) {

  int i;

  mpz_t rx1;
  mpz_t ry1;
  mpz_t rx2;
  mpz_t ry2;

  mpz_t t1;
  mpz_t t2;
  mpz_t s;

  mpz_t Ox;
  mpz_t Oy;

  mpz_init(rx1);
  mpz_init(ry1);
  mpz_init(rx2);
  mpz_init(ry2);

  mpz_init(Ox);
  mpz_init(Oy);

  mpz_init(t1);
  mpz_init(t2);
  mpz_init(s);

  mpz_set_si(Ox, -1);
  mpz_set_si(Oy, -1);

  /* Test processing of unit element. */
  ecn_jsquare_aff(t1, t2, s,
		  rx1, ry1,
		  modulus, a, b,
		  Ox, Oy);

  if (mpz_cmp_si(rx1, -1) != 0) {
    fprintf(stderr, "Doubling unit element failed!");
  }
  if (mpz_cmp_si(ry1, -1) != 0) {
    fprintf(stderr, "Doubling unit element failed!");
  }


  /* Test processing of general elements. */
  mpz_set(rx1, gx);
  mpz_set(ry1, gy);

  mpz_set(rx2, gx);
  mpz_set(ry2, gy);


  for (i = 0; i < 1000000; i++) {

    ecn_square(t1, t2, s,
	       rx1, ry1,
	       modulus, a, b,
	       rx1, ry1);

    ecn_jsquare_aff(t1, t2, s,
		    rx2, ry2,
		    modulus, a, b,
		    rx2, ry2);

    if (mpz_cmp(rx1, rx2) != 0) {
      fprintf(stderr, "General fail, wrong x coordinate!");
    }
    if (mpz_cmp(ry1, ry2) != 0) {
      fprintf(stderr, "General fail, wrong y coordinate!");
    }
  }

  mpz_clear(s);
  mpz_clear(t2);
  mpz_clear(t1);

  mpz_clear(ry2);
  mpz_clear(rx2);
  mpz_clear(ry1);
  mpz_clear(rx1);
}

void
test_mul(mpz_t modulus, mpz_t a, mpz_t b, mpz_t gx, mpz_t gy) {

  int i;

  mpz_t rx1;
  mpz_t ry1;
  mpz_t rx2;
  mpz_t ry2;

  mpz_t t1;
  mpz_t t2;
  mpz_t s;

  mpz_t Ox;
  mpz_t Oy;

  mpz_t tx;
  mpz_t ty;

  mpz_init(rx1);
  mpz_init(ry1);
  mpz_init(rx2);
  mpz_init(ry2);

  mpz_init(t1);
  mpz_init(t2);
  mpz_init(s);

  mpz_init(Ox);
  mpz_init(Oy);

  mpz_init(tx);
  mpz_init(ty);

  mpz_set_si(Ox, -1);
  mpz_set_si(Oy, -1);

  /* Test processing of unit element. */
  ecn_jmul_aff(t1, t2, s,
	       rx1, ry1,
	       modulus, a, b,
	       Ox, Oy,
	       Ox, Oy);

  if (mpz_cmp_si(rx1, -1) != 0) {
    fprintf(stderr, "Doubling unit element failed!");
  }
  if (mpz_cmp_si(ry1, -1) != 0) {
    fprintf(stderr, "Doubling unit element failed!");
  }

  ecn_jmul_aff(t1, t2, s,
	       rx1, ry1,
	       modulus, a, b,
	       gx, gy,
	       Ox, Oy);

  if (mpz_cmp(rx1, gx) != 0) {
    fprintf(stderr, "Right multiplication with unit element failed!");
  }
  if (mpz_cmp(ry1, gy) != 0) {
    fprintf(stderr, "Right multiplication with unit element failed!");
  }

  ecn_jmul_aff(t1, t2, s,
	       rx1, ry1,
	       modulus, a, b,
	       Ox, Oy,
	       gx, gy);

  if (mpz_cmp(rx1, gx) != 0) {
    fprintf(stderr, "Left multiplication with unit element failed!");
  }
  if (mpz_cmp(ry1, gy) != 0) {
    fprintf(stderr, "Left multiplication with unit element failed!");
  }


  /* Test forwarding to squaring code. */

  ecn_mul(t1, t2, s,
	  rx1, ry1,
	  modulus, a, b,
	  gx, gy,
	  gx, gy);

  ecn_jmul_aff(t1, t2, s,
	       rx2, ry2,
	       modulus, a, b,
	       gx, gy,
	       gx, gy);

  if (mpz_cmp(rx1, rx2) != 0) {
    fprintf(stderr, "Forwarding to squaring code failed!");
  }
  if (mpz_cmp(ry1, ry2) != 0) {
    fprintf(stderr, "Forwarding to squaring code failed!");
  }

  /* Test processing of general elements. */
  mpz_set(rx1, gx);
  mpz_set(ry1, gy);

  mpz_set(rx2, gx);
  mpz_set(ry2, gy);

  ecn_square(t1, t2, s,
	     tx, ty,
	     modulus, a, b,
	     gx, gy);


  for (i = 0; i < 1000000; i++) {

    ecn_mul(t1, t2, s,
	    rx1, ry1,
	    modulus, a, b,
	    rx1, ry1,
	    tx, ty);

    ecn_jmul_aff(t1, t2, s,
		 rx2, ry2,
		 modulus, a, b,
		 rx2, ry2,
		 tx, ty);

    if (mpz_cmp(rx1, rx2) != 0) {
      fprintf(stderr, "General fail, wrong x coordinate!");
    }
    if (mpz_cmp(ry1, ry2) != 0) {
      fprintf(stderr, "General fail, wrong y coordinate!");
    }

    ecn_square(t1, t2, s,
	       tx, ty,
	       modulus, a, b,
	       tx, ty);
  }

  mpz_clear(ty);
  mpz_clear(tx);

  mpz_init(Ox);
  mpz_init(Oy);

  mpz_clear(s);
  mpz_clear(t2);
  mpz_clear(t1);

  mpz_clear(ry2);
  mpz_clear(rx2);
  mpz_clear(ry1);
  mpz_clear(rx1);
}

void
test_exp(mpz_t modulus, mpz_t a, mpz_t b, mpz_t gx, mpz_t gy) {

  int i;
  int j;

  mpz_t rx1;
  mpz_t ry1;
  mpz_t rx2;
  mpz_t ry2;

  mpz_t t1;
  mpz_t t2;
  mpz_t s;

  mpz_t Ox;
  mpz_t Oy;

  mpz_t bx;
  mpz_t by;

  mpz_t exponent;

  mpz_init(rx1);
  mpz_init(ry1);
  mpz_init(rx2);
  mpz_init(ry2);

  mpz_init(t1);
  mpz_init(t2);
  mpz_init(s);

  mpz_init(Ox);
  mpz_init(Oy);

  mpz_init(bx);
  mpz_init(by);


  mpz_init(exponent);

  mpz_set_si(Ox, -1);
  mpz_set_si(Oy, -1);


  /* Test exponentiation of unit element. */
  mpz_set_ui(exponent, 1);
  mpz_mul_2exp(exponent, exponent, 100000);

  ecn_jexp_aff(rx1, ry1,
	       modulus, a, b,
	       Ox, Oy,
	       exponent);

  if (mpz_cmp_si(rx1, -1) != 0) {
    fprintf(stderr, "Exponentiation of unit element failed!");
  }
  if (mpz_cmp_si(ry1, -1) != 0) {
    fprintf(stderr, "Exponentiation of unit element failed!");
  }

  mpz_set(bx, gx);
  mpz_set(by, gy);

  for (i = 0; i < 100000; i++) {

    ecn_exp(rx1, ry1,
	    modulus, a, b,
	    bx, by,
	    exponent);

    ecn_jexp_aff(rx2, ry2,
    		 modulus, a, b,
    		 bx, by,
    		 exponent);

    if (mpz_cmp(rx1, rx2) != 0) {
      fprintf(stderr, "General fail, wrong x coordinate!");
    }
    if (mpz_cmp(ry1, ry2) != 0) {
      fprintf(stderr, "General fail, wrong y coordinate!");
    }

    if (mpz_cmp_si(rx1, -1) == 0) {
      mpz_set(bx, rx2);
      mpz_set(by, ry2);
    } else {
      mpz_set(bx, rx1);
      mpz_set(by, ry1);
    }

    mpz_mul(exponent, exponent, exponent);
    mpz_mod(exponent, exponent, modulus);

  }

  mpz_init(bx);
  mpz_init(by);

  mpz_init(Ox);
  mpz_init(Oy);

  mpz_clear(s);
  mpz_clear(t2);
  mpz_clear(t1);

  mpz_clear(ry2);
  mpz_clear(rx2);
  mpz_clear(ry1);
  mpz_clear(rx1);
}

void
test_sexp(mpz_t modulus, mpz_t a, mpz_t b, mpz_t gx, mpz_t gy) {

  int i;
  size_t len;

  mpz_t ropx1;
  mpz_t ropy1;

  mpz_t ropx2;
  mpz_t ropy2;

  mpz_t *basesx;
  mpz_t *basesy;
  mpz_t *exponents;

  mpz_t exponent;

  mpz_init(ropx1);
  mpz_init(ropy1);

  mpz_init(ropx2);
  mpz_init(ropy2);

  mpz_init(exponent);

  mpz_set_ui(exponent, 1);
  mpz_mul_2exp(exponent, exponent, 100000);
  mpz_mod(exponent, exponent, modulus);

  for (len = 100000; len < 100001; len += 1) {

    basesx = ecn_array_alloc_init(len);
    basesy = ecn_array_alloc_init(len);
    exponents = ecn_array_alloc_init(len);

    print_time();

    for (i = 0; i < len; i++) {
      ecn_exp(basesx[i], basesy[i],
	      modulus, a, b,
	      gx, gy,
	      exponent);

      mpz_mul(exponent, exponent, exponent);
      mpz_mod(exponent, exponent, modulus);

      mpz_set(exponents[i], exponent);
    }

    print_time();

    ecn_sexp(ropx1, ropy1,
	     basesx, basesy,
	     exponents,
	     len,
	     modulus, a, b);

    print_time();

    ecn_jsexp_aff(ropx2, ropy2,
		  basesx, basesy,
		  exponents,
		  len,
		  modulus, a, b);

    print_time();

    if (mpz_cmp(ropx1, ropx2) != 0) {
      fprintf(stderr, "FAIL x");
    }
    if (mpz_cmp(ropy1, ropy2) != 0) {
      fprintf(stderr, "FAIL y");
    }

    ecn_array_clear_dealloc(exponents, len);
    ecn_array_clear_dealloc(basesy, len);
    ecn_array_clear_dealloc(basesx, len);

  }

  mpz_clear(exponent);

}

void
test_ressol(mpz_t p) {

  int i;

  mpz_t a;
  mpz_t z;
  mpz_t res;

  mpz_init(a);
  mpz_init(z);
  mpz_init(res);

  mpz_set_si(a, 1);
  mpz_mul_2exp(a, a, 1000);
  mpz_mod(a, a, p);

  mpz_set_si(z, 2);
  while (mpz_legendre(z, p) == 1) {
    mpz_add_ui(z, z, 1);
  }

  mpz_mul(a, a, a);
  mpz_mod(a, a, p);

  for (i = 0; i < 10000000000; i++) {

    ecn_ressol(res, a, p);

    mpz_mul(res, res, res);
    mpz_mod(res, res, p);

    if (mpz_cmp(res, a) != 0) {
      fprintf(stderr, "FAIL");
      break;
    }

    /* Randomize a new square */
    mpz_powm(a, a, a, p);
    mpz_mul(a, a, a);
    mpz_mod(a, a, p);

  }

  mpz_clear(res);
  mpz_clear(z);
  mpz_clear(a);
}

int
main() {

  mpz_t modulus;
  mpz_t a;
  mpz_t b;
  mpz_t gx;
  mpz_t gy;
  mpz_t n;


  mpz_init(modulus);
  mpz_init(a);
  mpz_init(b);
  mpz_init(gx);
  mpz_init(gy);
  mpz_init(n);


  mpz_set_str(modulus, "0ffffffff00000001000000000000000000000000ffffffffffffffffffffffff", 16);
  mpz_set_str(a, "0ffffffff00000001000000000000000000000000fffffffffffffffffffffffc", 16);
  mpz_set_str(b, "05ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b", 16);
  mpz_set_str(gx, "06b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296", 16);
  mpz_set_str(gy, "04fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5", 16);
  mpz_set_str(n, "0ffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632551", 16);

  /*
  fprintf(stderr, "Testing squaring... ");
  test_square(modulus, a, b, gx, gy);
  fprintf(stderr, "done.\n");

  fprintf(stderr, "Testing multiplication... ");
  test_mul(modulus, a, b, gx, gy);
  fprintf(stderr, "done.\n");

  fprintf(stderr, "Testing exponentiation... ");
  test_exp(modulus, a, b, gx, gy);
  fprintf(stderr, "done.\n");

  fprintf(stderr, "Testing simultaneous exponentiation... ");
  test_sexp(modulus, a, b, gx, gy);
  fprintf(stderr, "done.\n");
*/
  fprintf(stderr, "Testing ressol... ");
  test_ressol(modulus);
  fprintf(stderr, "done.\n");

}
