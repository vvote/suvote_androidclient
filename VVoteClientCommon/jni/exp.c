/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <gmp.h>
#include "ecn.h"
#include <android/log.h>

#define LOGV(TAG,...) __android_log_print(ANDROID_LOG_VERBOSE, TAG,__VA_ARGS__)
#define LOGD(TAG,...) __android_log_print(ANDROID_LOG_DEBUG  , TAG,__VA_ARGS__)
#define LOGI(TAG,...) __android_log_print(ANDROID_LOG_INFO   , TAG,__VA_ARGS__)
#define LOGW(TAG,...) __android_log_print(ANDROID_LOG_WARN   , TAG,__VA_ARGS__)
#define LOGE(TAG,...) __android_log_print(ANDROID_LOG_ERROR  , TAG,__VA_ARGS__)
void
ecn_exp(mpz_t rx, mpz_t ry,
	mpz_t modulus, mpz_t a, mpz_t b,
	mpz_t x, mpz_t y,
	mpz_t exponent) {

  int i;
  mpz_t t1;
  mpz_t t2;
  mpz_t s;

  mpz_init(t1);
  mpz_init(t2);
  mpz_init(s);

  /* Initialize with the unit element. */
  mpz_set_si(rx, -1);
  mpz_set_si(ry, -1);

  /* Determine bit length. */
  int bitLength = (int)mpz_sizeinbase(exponent, 2);

  for (i = bitLength; i >= 0; i--) {

    /* Square. */
    ecn_square(t1, t2, s,
	       rx, ry,
	       modulus, a, b,
	       rx, ry);

    /* Multiply. */
    if (mpz_tstbit(exponent, i)) {

      ecn_mul(t1, t2, s,
	      rx, ry,
	      modulus, a, b,
	      rx, ry,
	      x, y);
    }
  }

  mpz_clear(s);
  mpz_clear(t2);
  mpz_clear(t1);

  return;
}
