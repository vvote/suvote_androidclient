/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <gmp.h>


typedef struct
{
  mpz_t modulus;          /**< Modulus used in computations. */
  mpz_t a;                /**< x-coefficient of curve. */
  mpz_t b;                /**< constant coefficient of curve. */
  size_t len;             /**< Total number of bases/exponents. */
  size_t block_width;     /**< Number of bases/exponents in each block. */
  size_t tabs_len;        /**< Number of blocks. */
  mpz_t **tabsx;          /**< Table of tables, one sub-table for each block. */
  mpz_t **tabsy;          /**< Table of tables, one sub-table for each block. */

} ecn_sexp_tab[1]; /* Magic references. */

typedef struct
{

  ecn_sexp_tab tab;      /**< Underlying simultaneous exponentiation table. */
  size_t slice_bit_len;   /**< Bit length of each slice. */

} ecn_fexp_tab[1]; /* Magic references. */


/**
 * Returns the square of the input point.
 *
 * @param t1 Temporary variable.
 * @param t2 Temporary variable.
 * @param s Temporary variable.
 * @param rx Destination of x-coordinate of result.
 * @param ry Destination of y-coordinate of result.
 * @param modulus Modulus of field.
 * @param a x-coefficient of curve.
 * @param b Constant coefficient of curve.
 * @param x x-coordinate of input point.
 * @param y y-coordinate of input point.
 */
void
ecn_square(mpz_t t1, mpz_t t2, mpz_t s,
	   mpz_t rx, mpz_t ry,
	   mpz_t modulus, mpz_t a, mpz_t b,
	   mpz_t x, mpz_t y);

/**
 * Returns the product of the input points.
 *
 * @param t1 Temporary variable.
 * @param t2 Temporary variable.
 * @param s Temporary variable.
 * @param rx Destination of x-coordinate of result.
 * @param ry Destination of y-coordinate of result.
 * @param modulus Modulus of field.
 * @param a x-coefficient of curve.
 * @param b Constant coefficient of curve.
 * @param x1 x-coordinate of first input point.
 * @param y1 y-coordinate of first input point.
 * @param x2 x-coordinate of second input point.
 * @param y2 y-coordinate of second input point.
 */
void
ecn_mul(mpz_t t1, mpz_t t2, mpz_t s,
	mpz_t rx, mpz_t ry,
	mpz_t modulus, mpz_t a, mpz_t b,
	mpz_t x1, mpz_t y1,
	mpz_t x2, mpz_t y2);

/**
 * Returns the power of a point to the given exponent. In contrast to
 * the squaring and multiplication functions, this function requires
 * the input and destination points to be distinct.
 *
 * @param rx Destination of x-coordinate of result.
 * @param ry Destination of y-coordinate of result.
 * @param modulus Modulus of field.
 * @param a x-coefficient of curve.
 * @param b Constant coefficient of curve.
 * @param x x-coordinate of input point.
 * @param y y-coordinate of input point.
 * @param exponent Exponent.
 */
void
ecn_exp(mpz_t rx, mpz_t ry,
	mpz_t modulus, mpz_t a, mpz_t b,
	mpz_t x, mpz_t y,
	mpz_t exponent);

/**
 * Initializes a table used for precomputation.
 *
 * @param table Table to be initialized.
 * @param modulus Modulus of field.
 * @param a x-coefficient of curve.
 * @param b Constant coefficient of curve.
 * @param len Total number of bases.
 * @param block_width Number of bases exponentiated simultaneously.
 */
void
ecn_sexp_init(ecn_sexp_tab table,
	      mpz_t modulus, mpz_t a, mpz_t b,
	      size_t len, size_t block_width);

/**
 * Releases the memory allocated by the pre-computed table.
 *
 * @param table Table to be initialized.
 */
void
ecn_sexp_clear(ecn_sexp_tab table);

/**
 * Performs pre-computation for the given table and bases.
 *
 * @param table Table to be initialized.
 * @param basesx x-coefficients of the bases.
 * @param basesy y-coefficients of the bases.
 */
void
ecn_sexp_precomp(ecn_sexp_tab table, mpz_t *basesx, mpz_t *basesy);

/**
 * Raises the bases of the pre-computed table simultaneously to the
 * given exponents.
 *
 * @param ropx Destination of x-coordinate of result.
 * @param ropy Destination of y-coordinate of result.
 * @param table Table to be initialized.
 * @param exponents Exponents.
 * @param max_exponent_bitlen Maximal number of bits in the exponents.
 */
void
ecn_sexp_table(mpz_t ropx, mpz_t ropy,
	       ecn_sexp_tab table,
	       mpz_t *exponents,
	       size_t max_exponent_bitlen);

void
ecn_sexp_block_batch(mpz_t ropx, mpz_t ropy,
		     mpz_t *basesx, mpz_t *basesy,
		     mpz_t *exponents,
		     size_t len,
		     mpz_t modulus, mpz_t a, mpz_t b,
		     size_t block_width, size_t batch_len,
		     size_t max_exponent_bitlen);

void
ecn_sexp(mpz_t ropx, mpz_t ropy,
	 mpz_t *basesx, mpz_t *basesy,
	 mpz_t *exponents,
	 size_t len,
	 mpz_t modulus, mpz_t a, mpz_t b);


typedef struct
{
  mpz_t modulus;          /**< Modulus used in computations. */
  mpz_t a;                /**< x-coefficient of curve. */
  mpz_t b;                /**< constant coefficient of curve. */
  size_t len;             /**< Total number of bases/exponents. */
  size_t block_width;     /**< Number of bases/exponents in each block. */
  size_t tabs_len;        /**< Number of blocks. */
  mpz_t **tabsx;          /**< Table of tables, one sub-table for each block. */
  mpz_t **tabsy;          /**< Table of tables, one sub-table for each block. */
  mpz_t **tabsz;          /**< Table of tables, one sub-table for each block. */

} ecn_jsexp_tab[1]; /* Magic references. */

typedef struct
{

  ecn_jsexp_tab tab;      /**< Underlying simultaneous exponentiation table. */
  size_t slice_bit_len;   /**< Bit length of each slice. */

} ecn_jfexp_tab[1]; /* Magic references. */



void
ecn_jaff(mpz_t X, mpz_t Y, mpz_t Z, mpz_t modulus);

void
ecn_affj(mpz_t X, mpz_t Y, mpz_t Z, mpz_t modulus);

/**
 *
 * @param S
 * @param M
 * @param T
 * @param X3
 * @param Y3
 * @param Z3
 * @param modulus
 * @param a
 * @param X1
 * @param Y1
 * @param Z1
 */
void
ecn_jsquare(mpz_t t1, mpz_t t2, mpz_t t3,
	    mpz_t S, mpz_t M, mpz_t T,
	    mpz_t X3, mpz_t Y3, mpz_t Z3,
	    mpz_t modulus, mpz_t a,
	    mpz_t X1, mpz_t Y1, mpz_t Z1);

void
ecn_jsquare_aff(mpz_t t1, mpz_t t2, mpz_t s,
		mpz_t rx, mpz_t ry,
		mpz_t modulus, mpz_t a, mpz_t b,
		mpz_t x, mpz_t y);

/**
 *
 * @param U1
 * @param U2
 * @param S1
 * @param S2
 * @param H
 * @param r
 * @param X3
 * @param Y3
 * @param Z3
 * @param modulus
 * @param a
 * @param X1
 * @param Y1
 * @param Z1
 * @param X2
 * @param Y2
 * @param Z2
 * @param Z2Z2
 * @param Z2Z2Z2
 */
void
ecn_jmul(mpz_t t1, mpz_t t2, mpz_t t3,
	 mpz_t U1, mpz_t U2,
	 mpz_t S1, mpz_t S2,
	 mpz_t H,  mpz_t r,
	 mpz_t X3, mpz_t Y3, mpz_t Z3,
	 mpz_t modulus, mpz_t a,
	 mpz_t X1, mpz_t Y1, mpz_t Z1,
	 mpz_t X2, mpz_t Y2, mpz_t Z2,
	 mpz_t Z2Z2, mpz_t Z2Z2Z2);

void
ecn_jmul_aff(mpz_t t1, mpz_t t2, mpz_t s,
	     mpz_t rx, mpz_t ry,
	     mpz_t modulus, mpz_t a, mpz_t b,
	     mpz_t x1, mpz_t y1,
	     mpz_t x2, mpz_t y2);

/**
 *
 *
 * @param RX
 * @param RY
 * @param RZ
 * @param modulus
 * @param a
 * @param X
 * @param Y
 * @param Z
 * @param exponent
 */
void
ecn_jexp(mpz_t RX, mpz_t RY, mpz_t RZ,
	 mpz_t modulus, mpz_t a,
	 mpz_t X, mpz_t Y, mpz_t Z,
	 mpz_t exponent);

/**
 * Returns the power of a point to the given exponent. In contrast to
 * the squaring and multiplication functions, this function requires
 * the input and destination points to be distinct.
 *
 * @param rx Destination of x-coordinate of result.
 * @param ry Destination of y-coordinate of result.
 * @param modulus Modulus of field.
 * @param a x-coefficient of curve.
 * @param b Constant coefficient of curve.
 * @param x x-coordinate of input point.
 * @param y y-coordinate of input point.
 * @param exponent Exponent.
 */
void
ecn_jexp_aff(mpz_t rx, mpz_t ry,
	     mpz_t modulus, mpz_t a, mpz_t b,
	     mpz_t x, mpz_t y,
	     mpz_t exponent);

/**
 * Initializes a table used for precomputation.
 *
 * @param table Table to be initialized.
 * @param modulus Modulus of field.
 * @param a x-coefficient of curve.
 * @param b Constant coefficient of curve.
 * @param len Total number of bases.
 * @param block_width Number of bases exponentiated simultaneously.
 */
void
ecn_jsexp_init(ecn_jsexp_tab table,
	       mpz_t modulus, mpz_t a, mpz_t b,
	       size_t len, size_t block_width);

/**
 * Releases the memory allocated by the pre-computed table.
 *
 * @param table Table to be initialized.
 */
void
ecn_jsexp_clear(ecn_jsexp_tab table);

/**
 * Performs pre-computation for the given table and bases.
 *
 * @param table Table to be initialized.
 * @param basesx x-coefficients of the bases.
 * @param basesy y-coefficients of the bases.
 * @param basesz z-coefficients of the bases.
 */
void
ecn_jsexp_precomp(ecn_jsexp_tab table, mpz_t *basesx, mpz_t *basesy,
		  mpz_t *basesz);

/**
 * Raises the bases of the pre-computed table simultaneously to the
 * given exponents.
 *
 * @param ropx Destination of x-coordinate of result.
 * @param ropy Destination of y-coordinate of result.
 * @param ropz Destination of z-coordinate of result.
 * @param table Table to be initialized.
 * @param exponents Exponents.
 * @param max_exponent_bitlen Maximal number of bits in the exponents.
 */
void
ecn_jsexp_table(mpz_t ropx, mpz_t ropy, mpz_t ropz,
		ecn_jsexp_tab table,
		mpz_t *exponents,
		size_t max_exponent_bitlen);

void
ecn_jsexp_block_batch(mpz_t ropx, mpz_t ropy, mpz_t ropz,
		      mpz_t *basesx, mpz_t *basesy, mpz_t *basesz,
		      mpz_t *exponents,
		      size_t len,
		      mpz_t modulus, mpz_t a, mpz_t b,
		      size_t block_width, size_t batch_len,
		      size_t max_exponent_bitlen);

void
ecn_jsexp(mpz_t ropx, mpz_t ropy, mpz_t ropz,
	  mpz_t *basesx, mpz_t *basesy, mpz_t *basesz,
	  mpz_t *exponents,
	  size_t len,
	  mpz_t modulus, mpz_t a, mpz_t b);

void
ecn_jsexp_aff(mpz_t ropx, mpz_t ropy,
	      mpz_t *basesx, mpz_t *basesy,
	      mpz_t *exponents,
	      size_t len,
	      mpz_t modulus, mpz_t a, mpz_t b);

mpz_t *
ecn_array_alloc(size_t len);

mpz_t *
ecn_array_alloc_init(size_t len);

void
ecn_array_clear_dealloc(mpz_t *a, size_t len);

void
ressol(mpz_t res, mpz_t a, mpz_t p);
