/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <gmp.h>
#include "ecn.h"

void
ecn_jexp(mpz_t RX, mpz_t RY, mpz_t RZ,
	 mpz_t modulus, mpz_t a,
	 mpz_t X, mpz_t Y, mpz_t Z,
	 mpz_t exponent) {

  int i;

  mpz_t t1;
  mpz_t t2;
  mpz_t t3;
  mpz_t t4;
  mpz_t t5;
  mpz_t t6;
  mpz_t t7;
  mpz_t t8;
  mpz_t t9;

  mpz_t ZZ;
  mpz_t ZZZ;

  mpz_init(t1);
  mpz_init(t2);
  mpz_init(t3);
  mpz_init(t4);
  mpz_init(t5);
  mpz_init(t6);
  mpz_init(t7);
  mpz_init(t8);
  mpz_init(t9);

  mpz_init(ZZ);
  mpz_init(ZZZ);

  /* Compute powers of Z */
  mpz_mul(ZZ, Z, Z);
  mpz_mod(ZZ, ZZ, modulus);

  mpz_mul(ZZZ, ZZ, Z);
  mpz_mod(ZZZ, ZZZ, modulus);

  /* Initialize with the unit element. */
  mpz_set_si(RX, 0);
  mpz_set_si(RY, 1);
  mpz_set_si(RZ, 0);

  /* Determine bit length. */
  int bitLength = (int)mpz_sizeinbase(exponent, 2);

  for (i = bitLength; i >= 0; i--) {

    /* Square. */
    ecn_jsquare(t1, t2, t3, t4, t5, t6,
		RX, RY, RZ,
		modulus, a,
		RX, RY, RZ);

    /* Multiply. */
    if (mpz_tstbit(exponent, i)) {

      ecn_jmul(t1, t2, t3,
	       t4, t5,
	       t6, t7,
	       t8, t9,
	       RX, RY, RZ,
	       modulus, a,
	       RX, RY, RZ,
	       X, Y, Z, ZZ, ZZZ);
    }
  }

  mpz_clear(ZZZ);
  mpz_clear(ZZ);

  mpz_clear(t9);
  mpz_clear(t8);
  mpz_clear(t7);
  mpz_clear(t6);
  mpz_clear(t5);
  mpz_clear(t4);
  mpz_clear(t3);
  mpz_clear(t2);
  mpz_clear(t1);

  return;
}
