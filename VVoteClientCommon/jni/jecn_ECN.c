/*
 * Copyright 2011 2012 Douglas Wikstrom
 *
 * This file is part of a package for JECN that provides native
 * elliptic curve code (ECN).
 *
 * JECN is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JECN is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JECN. If not, see <http://www.gnu.org/licenses/>.
 */

#include "jecn_ECN.h"
#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include "convert.h"
#include "ecn.h"

#ifdef __cplusplus
extern "C" {
#endif

#define JACOBI_COORDINATES

void mpz_t_pair_to_2DjbyteArray(JNIEnv *env, jobjectArray* res,
				mpz_t first, mpz_t second) {

  jbyteArray firstArray;
  jbyteArray secondArray;

  // Get the byte array class
  jclass byteArrayClass = (*env)->FindClass(env, "[B");

  *res = (*env)->NewObjectArray(env, (jsize)2, byteArrayClass, NULL);

  mpz_t_to_jbyteArray(env, &firstArray, first);
  mpz_t_to_jbyteArray(env, &secondArray, second);

  (*env)->SetObjectArrayElement(env, *res, (jsize)0, firstArray);
  (*env)->SetObjectArrayElement(env, *res, (jsize)1, secondArray);
}

JNIEXPORT jobjectArray JNICALL Java_jecn_ECN_exp
  (JNIEnv *env, jclass clazz, jbyteArray javaModulus, jbyteArray javaA,
   jbyteArray javaB, jbyteArray javaX, jbyteArray javaY,
   jbyteArray javaExponent)
{
  mpz_t modulus;
  mpz_t a;
  mpz_t b;
  mpz_t x;
  mpz_t y;
  mpz_t exponent;

  mpz_t rx;
  mpz_t ry;

  jobjectArray javaResult;

  /* Translate jbyteArray-parameters to their corresponding GMP
     mpz_t-elements. */
  jbyteArray_to_mpz_t(env, &modulus, javaModulus);
  jbyteArray_to_mpz_t(env, &a, javaA);
  jbyteArray_to_mpz_t(env, &b, javaB);
  jbyteArray_to_mpz_t(env, &x, javaX);
  jbyteArray_to_mpz_t(env, &y, javaY);
  jbyteArray_to_mpz_t(env, &exponent, javaExponent);

  /* Compute exponentiation. */

  mpz_init(rx);
  mpz_init(ry);

#ifdef JACOBI_COORDINATES

  ecn_jexp_aff(rx, ry, modulus, a, b, x, y, exponent);

#else

  ecn_exp(rx, ry, modulus, a, b, x, y, exponent);

#endif

  mpz_t_pair_to_2DjbyteArray(env, &javaResult, rx, ry);

  /* Deallocate resources. */
  mpz_clear(ry);
  mpz_clear(rx);

  mpz_clear(exponent);
  mpz_clear(y);
  mpz_clear(x);
  mpz_clear(b);
  mpz_clear(a);
  mpz_clear(modulus);

  return javaResult;
}

/*
 * Class:     jecn_ECN
 * Method:    ressol
 * Signature: ([B[B)[B
 */
JNIEXPORT jbyteArray JNICALL Java_jecn_ECN_ressol
(JNIEnv *env, jclass clazz, jbyteArray javaa, jbyteArray javaModulus) {

  jbyteArray javaResult;

  mpz_t a;
  mpz_t modulus;
  mpz_t result;

  mpz_init(result);

  jbyteArray_to_mpz_t(env, &a, javaa);
  jbyteArray_to_mpz_t(env, &modulus, javaModulus);

  ecn_ressol(result, a, modulus);

  mpz_t_to_jbyteArray(env, &javaResult, result);

  mpz_clear(result);
  mpz_clear(a);
  mpz_clear(modulus);

  return javaResult;
}

JNIEXPORT jdouble JNICALL Java_jecn_ECN_testme(JNIEnv * env, jobject obj, jdouble SO)
{
  return 0.0;
}

/*
 * Class:     jecn_ECN
 * Method:    mul
 * Signature: ([B[B[B[B[B[B[B)[[B
 */
JNIEXPORT jobjectArray JNICALL Java_jecn_ECN_mul
(JNIEnv *env, jclass clazz,
 jbyteArray javaModulus, jbyteArray javaA, jbyteArray javaB,
 jbyteArray javax1, jbyteArray javay1,
 jbyteArray javax2, jbyteArray javay2) {

  jobjectArray javaResult;

  mpz_t modulus;
  mpz_t a;
  mpz_t b;
  mpz_t x1;
  mpz_t y1;
  mpz_t x2;
  mpz_t y2;
  mpz_t resultx;
  mpz_t resulty;

  mpz_t t1;
  mpz_t t2;
  mpz_t s;

  /* Convert curve parameters represented as byte[] to mpz_t. */
  jbyteArray_to_mpz_t(env, &modulus, javaModulus);
  jbyteArray_to_mpz_t(env, &a, javaA);
  jbyteArray_to_mpz_t(env, &b, javaB);

  /* Convert point coordinates as byte[] to mpz_t */
  jbyteArray_to_mpz_t(env, &x1, javax1);
  jbyteArray_to_mpz_t(env, &y1, javay1);
  jbyteArray_to_mpz_t(env, &x2, javax2);
  jbyteArray_to_mpz_t(env, &y2, javay2);

  mpz_init(resultx);
  mpz_init(resulty);

  mpz_init(t1);
  mpz_init(t2);
  mpz_init(s);

  ecn_mul(t1, t2, s,
	  resultx, resulty,
	  modulus, a, b,
	  x1, y1,
	  x2, y2);

  mpz_t_pair_to_2DjbyteArray(env, &javaResult, resultx, resulty);

  mpz_clear(s);
  mpz_clear(t2);
  mpz_clear(t1);

  mpz_clear(y2);
  mpz_clear(x2);
  mpz_clear(y1);
  mpz_clear(x1);
  mpz_clear(resultx);
  mpz_clear(resulty);
  mpz_clear(b);
  mpz_clear(a);
  mpz_clear(modulus);

  return javaResult;
}


/*
 * Class:     jecn_ECN
 * Method:    sexp
 * Signature: ([B[B[B[[B[[B[[B)[[B
 */
JNIEXPORT jobjectArray JNICALL Java_jecn_ECN_sexp
  (JNIEnv *env, jclass clazz,
   jbyteArray javaModulus, jbyteArray javaA, jbyteArray javaB,
   jobjectArray javaBasesx, jobjectArray javaBasesy,
   jobjectArray javaExponents) {

  int i;
  mpz_t *basesx;
  mpz_t *basesy;
  mpz_t *exponents;
  mpz_t modulus;
  mpz_t a;
  mpz_t b;
  mpz_t resultx;
  mpz_t resulty;

  /* Extract number of bases/exponents. */
  jsize numberOfBases = (*env)->GetArrayLength(env, javaBasesx);

  jobjectArray javaResult;

  /* Convert bases represented arrays of byte[] to arrays of mpz_t. */
  basesx = ecn_array_alloc(numberOfBases);
  basesy = ecn_array_alloc(numberOfBases);
  for (i = 0; i < numberOfBases; i++)
    {
      jbyteArray javaBasex =
	(jbyteArray)(*env)->GetObjectArrayElement(env, javaBasesx, i);
      jbyteArray_to_mpz_t(env, &(basesx[i]), javaBasex);

      jbyteArray javaBasey =
	(jbyteArray)(*env)->GetObjectArrayElement(env, javaBasesy, i);
      jbyteArray_to_mpz_t(env, &(basesy[i]), javaBasey);
    }

  /* Convert exponents represented as array of byte[] to an array of
     mpz_t. */
  exponents = ecn_array_alloc(numberOfBases);
  for (i = 0; i < numberOfBases; i++)
    {
      jbyteArray javaExponent =
	(jbyteArray)(*env)->GetObjectArrayElement(env, javaExponents, i);
      jbyteArray_to_mpz_t(env, &(exponents[i]), javaExponent);
    }

  /* Convert curve parameters represented as byte[] to mpz_t. */
  jbyteArray_to_mpz_t(env, &modulus, javaModulus);
  jbyteArray_to_mpz_t(env, &a, javaA);
  jbyteArray_to_mpz_t(env, &b, javaB);

  /* Call GMPMEE's exponentiated product function. */
  mpz_init(resultx);
  mpz_init(resulty);


#ifdef JACOBI_COORDINATES

  ecn_jsexp_aff(resultx, resulty,
		basesx, basesy,
		exponents,
		numberOfBases,
		modulus, a, b);

#else

  ecn_sexp(resultx, resulty,
  	   basesx, basesy,
  	   exponents,
  	   numberOfBases,
  	   modulus, a, b);

#endif

  mpz_t_pair_to_2DjbyteArray(env, &javaResult, resultx, resulty);

  /* Deallocate resources. */
  mpz_clear(resultx);
  mpz_clear(resulty);
  mpz_clear(b);
  mpz_clear(a);
  mpz_clear(modulus);
  ecn_array_clear_dealloc(exponents, numberOfBases);
  ecn_array_clear_dealloc(basesy, numberOfBases);
  ecn_array_clear_dealloc(basesx, numberOfBases);

  return javaResult;
}

int
fexp_block_width(int bit_length, int size) {

  int width = 2;
  double cost = 1.5 * bit_length;
  double oldCost;
  do {

    oldCost = cost;

    // Amortized cost for table.
    double t = ((1 << width) - width + bit_length) / size;

    // Cost for multiplication.
    double m = (bit_length / width);

    cost = t + m;

    width++;

  } while (width < 17 && cost < oldCost);

  // We reduce the theoretical value by one to account for the
  // overhead.
  return width - 1;
}


#ifdef JACOBI_COORDINATES

/*
 * Class:     jecn_ECN
 * Method:    fexp_precompute
 * Signature: ([B[B[B[B[BII)J
 */
JNIEXPORT jlong JNICALL Java_jecn_ECN_fexp_1precompute
  (JNIEnv *env, jclass clazz,
   jbyteArray javaModulus, jbyteArray javaA, jbyteArray javaB,
   jbyteArray javaBasisx, jbyteArray javaBasisy, jint bitLength, jint size) {

  int i;
  int block_width;
  mpz_t modulus;
  mpz_t a;
  mpz_t b;

  mpz_t basisx;
  mpz_t basisy;
  mpz_t basisz;
  mpz_t exponent;

  mpz_t *basesx;
  mpz_t *basesy;
  mpz_t *basesz;

  mpz_t t1;
  mpz_t t2;
  mpz_t s;

  ecn_jfexp_tab *tablePtr =
    (ecn_jfexp_tab *)malloc(sizeof(ecn_jfexp_tab));

  /* Convert curve parameters represented as byte[] to mpz_t. */
  jbyteArray_to_mpz_t(env, &modulus, javaModulus);
  jbyteArray_to_mpz_t(env, &a, javaA);
  jbyteArray_to_mpz_t(env, &b, javaB);

  jbyteArray_to_mpz_t(env, &basisx, javaBasisx);
  jbyteArray_to_mpz_t(env, &basisy, javaBasisy);
  mpz_init(basisz);

  mpz_init(exponent);

  block_width = fexp_block_width((int)bitLength, (int)size);

  (*tablePtr)->slice_bit_len =
    (((int)bitLength) + (block_width - 1)) / block_width;

  ecn_jsexp_init((*tablePtr)->tab, modulus, a, b, block_width, block_width);

  basesx = ecn_array_alloc_init(block_width);
  basesy = ecn_array_alloc_init(block_width);
  basesz = ecn_array_alloc_init(block_width);

  mpz_set_ui(exponent, 1);
  mpz_mul_2exp(exponent, exponent, (*tablePtr)->slice_bit_len);

  mpz_set(basesx[0], basisx);
  mpz_set(basesy[0], basisy);
  mpz_set_si(basesz[0], 1);

  ecn_affj(basesx[0], basesy[0], basesz[0], modulus);

  for (i = 1; i < block_width; i++) {

    ecn_jexp(basesx[i], basesy[i], basesz[i],
	     modulus, a,
	     basesx[i - 1], basesy[i - 1], basesz[i - 1],
	     exponent);
  }

  ecn_jsexp_precomp((*tablePtr)->tab, basesx, basesy, basesz);

  ecn_array_clear_dealloc(basesz, block_width);
  ecn_array_clear_dealloc(basesy, block_width);
  ecn_array_clear_dealloc(basesx, block_width);

  mpz_clear(exponent);
  mpz_clear(basisz);
  mpz_clear(basisy);
  mpz_clear(basisx);

  mpz_clear(b);
  mpz_clear(a);
  mpz_clear(modulus);

  return (jlong)(long)tablePtr;
}

/*
 * Class:     jecn_ECN
 * Method:    fexp
 * Signature: (J[B)[[B
 */
JNIEXPORT jobjectArray JNICALL Java_jecn_ECN_fexp
(JNIEnv *env, jclass clazz, jlong javaTablePtr, jbyteArray javaExponent) {

  int i;
  mpz_t exponent;
  mpz_t *exponents;
  ecn_jfexp_tab *tablePtr = (ecn_jfexp_tab*)(void*)javaTablePtr;
  int block_width = (*tablePtr)->tab->block_width;

  mpz_t resultx;
  mpz_t resulty;
  mpz_t resultz;

  jobjectArray javaResult;

  jbyteArray_to_mpz_t(env, &exponent, javaExponent);
  exponents = ecn_array_alloc_init(block_width);

  for (i = 0; i < block_width; i++) {
    mpz_tdiv_r_2exp(exponents[i], exponent, (*tablePtr)->slice_bit_len);
    mpz_tdiv_q_2exp(exponent, exponent, (*tablePtr)->slice_bit_len);
  }

  mpz_init(resultx);
  mpz_init(resulty);
  mpz_init(resultz);

  ecn_jsexp_table(resultx, resulty, resultz,
		  (*tablePtr)->tab,
		  exponents,
		  (*tablePtr)->slice_bit_len);

  ecn_jaff(resultx, resulty, resultz, (*tablePtr)->tab->modulus);

  mpz_t_pair_to_2DjbyteArray(env, &javaResult, resultx, resulty);

  mpz_clear(resultz);
  mpz_clear(resulty);
  mpz_clear(resultx);
  ecn_array_clear_dealloc(exponents, block_width);
  mpz_clear(exponent);

  return javaResult;
}

/*
 * Class:     jecn_ECN
 * Method:    fexp_clear
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_jecn_ECN_fexp_1clear
(JNIEnv *env, jclass clazz, jlong javaTablePtr) {

  ecn_jfexp_tab *tablePtr = (ecn_jfexp_tab*)(void*)javaTablePtr;
  ecn_jsexp_clear((*tablePtr)->tab);
  free(tablePtr);
}

#else

/*
 * Class:     jecn_ECN
 * Method:    fexp_precompute
 * Signature: ([B[B[B[B[BII)J
 */
JNIEXPORT jlong JNICALL Java_jecn_ECN_fexp_1precompute
  (JNIEnv *env, jclass clazz,
   jbyteArray javaModulus, jbyteArray javaA, jbyteArray javaB,
   jbyteArray javaBasisx, jbyteArray javaBasisy, jint bitLength, jint size) {

  int i;
  int block_width;
  mpz_t modulus;
  mpz_t a;
  mpz_t b;

  mpz_t basisx;
  mpz_t basisy;
  mpz_t exponent;

  mpz_t *basesx;
  mpz_t *basesy;

  mpz_t t1;
  mpz_t t2;
  mpz_t s;

  ecn_fexp_tab *tablePtr =
    (ecn_fexp_tab *)malloc(sizeof(ecn_fexp_tab));

  /* Convert curve parameters represented as byte[] to mpz_t. */
  jbyteArray_to_mpz_t(env, &modulus, javaModulus);
  jbyteArray_to_mpz_t(env, &a, javaA);
  jbyteArray_to_mpz_t(env, &b, javaB);

  jbyteArray_to_mpz_t(env, &basisx, javaBasisx);
  jbyteArray_to_mpz_t(env, &basisy, javaBasisy);
  mpz_init(exponent);

  block_width = fexp_block_width((int)bitLength, (int)size);

  (*tablePtr)->slice_bit_len =
    (((int)bitLength) + (block_width - 1)) / block_width;

  ecn_sexp_init((*tablePtr)->tab, modulus, a, b, block_width, block_width);

  basesx = ecn_array_alloc_init(block_width);
  basesy = ecn_array_alloc_init(block_width);

  mpz_set_ui(exponent, 1);
  mpz_mul_2exp(exponent, exponent, (*tablePtr)->slice_bit_len);

  mpz_set(basesx[0], basisx);
  mpz_set(basesy[0], basisy);

  for (i = 1; i < block_width; i++) {

    ecn_exp(basesx[i], basesy[i],
	    modulus, a, b,
	    basesx[i - 1], basesy[i - 1],
	    exponent);
  }

  ecn_sexp_precomp((*tablePtr)->tab, basesx, basesy);

  ecn_array_clear_dealloc(basesy, block_width);
  ecn_array_clear_dealloc(basesx, block_width);

  mpz_clear(exponent);
  mpz_clear(basisy);
  mpz_clear(basisx);

  mpz_clear(b);
  mpz_clear(a);
  mpz_clear(modulus);

  return (jlong)(long)tablePtr;
}

/*
 * Class:     jecn_ECN
 * Method:    fexp
 * Signature: (J[B)[[B
 */
JNIEXPORT jobjectArray JNICALL Java_jecn_ECN_fexp
(JNIEnv *env, jclass clazz, jlong javaTablePtr, jbyteArray javaExponent) {

  int i;
  mpz_t exponent;
  mpz_t *exponents;
  ecn_fexp_tab *tablePtr = (ecn_fexp_tab*)(void*)javaTablePtr;
  int block_width = (*tablePtr)->tab->block_width;

  mpz_t resultx;
  mpz_t resulty;

  jobjectArray javaResult;

  jbyteArray_to_mpz_t(env, &exponent, javaExponent);
  exponents = ecn_array_alloc_init(block_width);

  for (i = 0; i < block_width; i++) {
    mpz_tdiv_r_2exp(exponents[i], exponent, (*tablePtr)->slice_bit_len);
    mpz_tdiv_q_2exp(exponent, exponent, (*tablePtr)->slice_bit_len);
  }

  mpz_init(resultx);
  mpz_init(resulty);

  ecn_sexp_table(resultx, resulty,
		 (*tablePtr)->tab,
		 exponents,
		 (*tablePtr)->slice_bit_len);

  mpz_t_pair_to_2DjbyteArray(env, &javaResult, resultx, resulty);

  mpz_clear(resulty);
  mpz_clear(resultx);
  ecn_array_clear_dealloc(exponents, block_width);
  mpz_clear(exponent);

  return javaResult;
}

/*
 * Class:     jecn_ECN
 * Method:    fexp_clear
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_jecn_ECN_fexp_1clear
(JNIEnv *env, jclass clazz, jlong javaTablePtr) {

  ecn_fexp_tab *tablePtr = (ecn_fexp_tab*)(void*)javaTablePtr;
  ecn_sexp_clear((*tablePtr)->tab);
  free(tablePtr);
}

#endif


#ifdef __cplusplus
}
#endif
