package android.content.pm;

/**
 * Reinplementation of internal android interface to allow us to tap into the package delete observer to allow us to delet the
 * training mode app.
 * 
 * @author Chris Culnane
 * 
 */
public interface IPackageDeleteObserver extends android.os.IInterface {

  public abstract static class Stub extends android.os.Binder implements android.content.pm.IPackageDeleteObserver {

    public static android.content.pm.IPackageDeleteObserver asInterface(android.os.IBinder obj) {
      throw new RuntimeException("Stub!");
    }

    public Stub() {
      throw new RuntimeException("Stub!");
    }

    @Override
    public android.os.IBinder asBinder() {
      throw new RuntimeException("Stub!");
    }

    @Override
    public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags)
        throws android.os.RemoteException {
      throw new RuntimeException("Stub!");
    }
  }

  public abstract void packageDeleted(java.lang.String packageName, int returnCode) throws android.os.RemoteException;
}
