/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.android;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;

/**
 * Connectivity check performs an HTTP check to verify whether a working networking connection is available. Due to the network
 * access requirement it has to be run in the background.
 * 
 * @author Chris Culnane
 * 
 */
public class ConnectivityCheck extends AsyncTask<URL, Integer, Boolean> {

  /**
   * Logger
   */
  private static final Logger logger       = LoggerFactory.getLogger(ConnectivityCheck.class);
  /**
   * Maximum amount of data to read from the test URL
   */
  private static final int    MAX_READ     = 524288;

  /**
   * Timeout length in ms
   */
  private static final int    READ_TIMEOUT = 15000;

  /**
   * Reference to ProgressDialog to show when trying to connect
   */
  private ProgressDialog      pd;

  /**
   * Determines whether a return to home should be shown
   */
  private boolean             showHome;

  /**
   * Reference to VECHome to call to return home
   */
  private VECHome             homeApp;

  /**
   * Post launchURL - where to go if a network connection is avaiable
   */
  private String              postLaunchURL;

  /**
   * Creates a new ConnectivityCheck AsyncTask to check if a network connection is up and if so connect to the postLaunchURL
   * 
   * @param homeApp
   *          VECHome that is the home app
   * @param postLaunchURL
   *          String containing the URL to go to after a successful check
   * @param showHome
   *          boolean, true to show the floating notification, false to not
   */
  public ConnectivityCheck(VECHome homeApp, String postLaunchURL, boolean showHome) {
    this.homeApp = homeApp;
    this.postLaunchURL = postLaunchURL;
    this.showHome = showHome;
  }

  /**
   * Cancels the async task
   */
  @Override
  protected void onCancelled() {
    super.onCancelled();
  }

  /**
   * Called before the actual check is performed. This creates a ProgressDialog and starts showing it
   */
  @Override
  protected void onPreExecute() {

    super.onPreExecute();
    logger.info("Showing connectivity check");
    pd = new ProgressDialog(homeApp);
    pd.setIndeterminate(true);
    pd.setCanceledOnTouchOutside(false);
    pd.setMessage("Checking connectivity...");
    pd.show();
  }

  /**
   * Where the actual check is performed
   * 
   * @param urls
   *          URL to try and access to perform the connectivity check
   * @return Boolean true if the specified URL was connected to, false if not
   */
  protected Boolean doInBackground(URL... urls) {

    if (urls.length == 1) {
      logger.info("About to check URL");
      HttpURLConnection urlConnection = null;
      ByteArrayOutputStream baos = null;
      try {
        urlConnection = (HttpURLConnection) urls[0].openConnection();
        urlConnection.setReadTimeout(READ_TIMEOUT);
        byte[] buf = new byte[1024];
        BufferedInputStream in = new BufferedInputStream(urlConnection.getInputStream());
        baos = new ByteArrayOutputStream();
        int read = 0;
        while ((read = in.read(buf)) != -1) {
          baos.write(buf, 0, read);
          if (baos.size() > MAX_READ) {
            logger.warn("Exceeded max read - this could be a denial of service");
            return false;
          }
        }
        logger.info("Data read, connectivity success");
        return true;

      }
      catch (IOException e) {
        logger.warn("Exception whilst reading - connectivity failed", e);
        return false;

      }

      finally {
        if (baos != null) {
          try {
            baos.close();
          }
          catch (IOException e) {
            logger.warn("Exception closing stream");
          }
        }
        if (urlConnection != null) {
          urlConnection.disconnect();
        }
      }

    }
    else {
      return false;
    }
  }

  /**
   * Called to update the progress update, but not implemented in this task because the progress is indeterminate
   */
  protected void onProgressUpdate(Integer... progress) {

  }

  /**
   * Called if the task is cancelled
   */
  @Override
  protected void onCancelled(Boolean result) {
    super.onCancelled(result);
  }

  /**
   * Called once the task has finished
   */
  @Override
  protected void onPostExecute(Boolean result) {
    super.onPostExecute(result);

    if (pd != null) {
      pd.dismiss();
    }
    if (result == false) {

      AlertDialog.Builder builder = new AlertDialog.Builder(this.homeApp);

      builder.setTitle("Connect?");
      builder.setMessage("No F5 connectivity has been detected. Do you want to launch the F5 connection tool?");

      builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

        public void onClick(DialogInterface dialog, int which) {
          // Do nothing but close the dialog
          homeApp.connectivityResult(false, true, postLaunchURL, showHome);

        }

      });

      builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
          homeApp.connectivityResult(false, false, postLaunchURL, showHome);

        }
      });

      AlertDialog alert = builder.create();
      alert.show();
    }
    homeApp.connectivityResult(result, false, postLaunchURL, showHome);

  }
}