/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.android;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListPopupWindow;

/**
 * FloatService displays a FloatingNotification that overlays all apps and is always available
 * 
 * @author Chris Culnane
 * 
 */
public class FloatingService extends Service {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(FloatingService.class);
  /**
   * WindowManager to create notification
   */
  private WindowManager       windowManager;
  /**
   * ImageView containing the actual floating notification icon
   */
  private ImageView           floatingNotification;

  /**
   * Constructs a new FloatingService
   */
  public FloatingService() {

  }

  /**
   * Called when the new Service is being created
   */
  @Override
  public void onCreate() {
    super.onCreate();
    logger.info("Creating new FloatingService");
    windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

    floatingNotification = new ImageView(this);

    floatingNotification.setImageResource(R.drawable.homeicon);
    final LayoutParams params = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT,
        WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_PHONE,
        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);
    params.gravity = Gravity.TOP | Gravity.LEFT;
    params.x = 0;
    params.y = 100;
    floatingNotification.setImageAlpha(150);
    windowManager.addView(floatingNotification, params);
    final FloatingNotificationListener listener = new FloatingNotificationListener(windowManager, params, floatingNotification);
    floatingNotification.setOnTouchListener(listener);
    floatingNotification.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View arg0) {
        if (!listener.shouldIgnore()) {
          initiatePopupWindow(floatingNotification);
        }
      }
    });
    logger.info("Showing FloatingNotification");
  }

  /**
   * When the service is destroyed we remove the floating notification view
   */
  @Override
  public void onDestroy() {

    super.onDestroy();
    if (floatingNotification != null)
      logger.info("Removing FloatingNotification");
    windowManager.removeView(floatingNotification);
  }

  /*
   * (non-Javadoc)
   * 
   * @see android.app.Service#onBind(android.content.Intent)
   */
  @Override
  public IBinder onBind(Intent arg0) {
    logger.warn("On bind called in FloatingService - not supported");
    throw new UnsupportedOperationException("Not yet implemented");
  }

  /**
   * Called when the notification is touched. This displays a popup dialog with a list of options for the user to select
   * 
   * @param anchor
   *          View that anchors this dialog - in this case the FloatingNotification
   */
  public void initiatePopupWindow(View anchor) {

    final ListPopupWindow popup = new ListPopupWindow(this);
    popup.setAnchorView(anchor);
    popup.setWidth(400);
    final ArrayList<String> options = new ArrayList<String>();
    options.add("Home");
    options.add("");
    options.add("Exit");
    popup.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.row, options));
    popup.setOnItemClickListener(new OnItemClickListener() {

      @Override
      public void onItemClick(AdapterView<?> arg0, View view, int position, long id3) {
        if (position < options.size()) {
          String selected = options.get(position);
          if (selected.equals("Home")) {
            logger.info("Home selected");
            popup.dismiss();
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);

          }
          else if (selected.equals("Exit")) {
            logger.info("Exit selected");
            popup.dismiss();
            FloatingService.this.stopSelf();

          }
          else if (selected.equals("")) {
            // do nothing, just a spacer
          }
          else {
            logger.warn("The option selected ({}) is unknown", selected);
          }
        }
      }

    });
    popup.show();

  }

}
