LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := libgmp
LOCAL_SRC_FILES := libgmp.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libpbc
LOCAL_SRC_FILES := libpbc.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libjnidispatch
LOCAL_SRC_FILES := libjnidispatch.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libjpbc-pbc
LOCAL_SRC_FILES := libjpbc-pbc.so
include $(PREBUILT_SHARED_LIBRARY)

