# vVoteAndroid #

## Summary ##
The vVoteAndroid repository contains a number of Android projects related to the vVote System. Each project can be checked out independently. The following provides a brief summary of the various projects and what they do:

### VVoteClient ###
VVoteClient runs as stick service in the background to provide proxy servers to communicate with the various back-end components. It also manages the interactions with the tablet, for example, scanning the barcode with the webcam and printing to the Epson printer. It performs the signature generation and verification for client communications and constructs the various messages. It is heavily dependent on the VVoteClientCommon Android library that provides the common functionality between VVoteClient and VVoteClientTraining.

### VVoteClientTraining ###
VVoteClientTraining provides an offline training mode of the VVoteClient. This allows training to be performed on a tablet with the standard front-end interface and equipment. The only difference is that all requests are handled locally and processed without communicating with a WBB. This allows for easy training, as well as not risking training messages being accidentally sent to a production WBB.

### VVoteClientCommon ###
VVoteClientCommon is an Android library project that provides the common functionality for VVoteClient and VVoteClientCommon. It contains the code necessary for communication with the Epson receipt printer and reading the QR Code via the WebCam. 

### VECHome ###
VECHome provides a replacement Home screen launcher for Android on the vVote tablets. This version of VECHome provides diagnostic and setup information as well as locking down the tablet to prevent access to other apps and settings on the tablet. It also forces the tablet into full screen mode and disables the virtual hardware buttons (like the home screen). It also manages the running and notifications from the DownloadBundleService.

### DownloadBundleService ###
The DownloadBundleService runs in the background, periodically checking for an available election data bundle to download. If a bundle is available it will download it and check it is valid. It then stops checking and stops the service.

### BLSSignatureChecker-native ###
This was derived from the BLSSignatureChecker with the addition of native optimisations for the BLS Signature Checking. This involved cross-compiling the [PBC Library](http://crypto.stanford.edu/pbc/) to run on Android as well as getting a version of the [GMP Library](https://gmplib.org/) to run on Android. We then cross-compiled the [JPBC Library](http://gas.dia.unisa.it/projects/jpbc/) to provide access to the native implementation. The app uses an HTML front-end that makes JavaScript calls to the underlying app to perform the actual signature checking. We utilise the BarcodeScanner app from ZXing to provide barcode scanning.

### android-webcam-library ###
The Android-webcam-library is a fork of [Open XC Library](https://github.com/openxc/android-webcam) with some minor modifications to provide a greater level of robustness and handle disconnect/reconnect of the USB webcam. 

### BLSSignatureChecker ###
This is a pure Java/Android BLS Signature Checker. This is no longer in active development due to performance issues with calculating a Pairing value on Android devices. It remains in the repository as a fall-back should we have problems with the native alternative. The native alternative was derived from this project, so it would be quick and easy to include the recent updates to the native checker if required.
