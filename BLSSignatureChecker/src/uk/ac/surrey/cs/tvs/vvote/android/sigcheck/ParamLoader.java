/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.android.sigcheck;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.CurveParams;
import android.app.ProgressDialog;
import android.os.AsyncTask;

/**
 * Simple AsyncTask to initialise the BLS Curve Parameters in a background thread and display a progress dialog whilst doing it.
 * 
 * @author Chris Culnane
 * 
 */
public class ParamLoader extends AsyncTask<Void, Void, Void> {

  /**
   * ProgressDialog to display whilst initialising CurveParams
   */
  private ProgressDialog pd;

  /**
   * Creates a new ParamLoader AsyncTask with the specified ProgressDialog
   * 
   * @param pd
   *          ProgressDialog to display whilst initialising parameters
   */
  public ParamLoader(ProgressDialog pd) {
    this.pd = pd;
  }

  /**
   * Load the actual parameters
   */
  @Override
  protected Void doInBackground(Void... arg0) {
    CurveParams.getInstance();
    return null;
  }

  /**
   * Having loaded the parameters hide the progress dialog
   */
  @Override
  protected void onPostExecute(Void result) {
    super.onPostExecute(result);
    this.pd.dismiss();
  }

  /**
   * Prior to executing the long task show the progress dialog
   */
  @Override
  protected void onPreExecute() {

    super.onPreExecute();
    this.pd.show();
  }

}
