/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.android.sigcheck;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.vvote.json.JSONException;
import org.vvote.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPublicKey;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import android.app.Activity;
import android.app.ProgressDialog;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.google.zxing.integration.android.IntentIntegrator;

/**
 * JavaScriptInterface that provides a way for JavaScript in the WebView to call Android methods. The HTML/JavaScript UI makes calls
 * to get a barcode and check a signature via this interface
 * 
 * @author Chris Culnane
 * 
 */
public class JavaScriptInterface {

  /**
   * Reference to the Activity we are running in
   */
  private Activity         activity;

  /**
   * IntentIntegrator to use for calling the BarcodeScanner
   */
  private IntentIntegrator integrator;

  /**
   * Underlying WebView that is being used, we need this reference to make return calls after checking the signature
   */
  private WebView          webView;

  /**
   * Constructs a new JavaScriptInterface with the specified Activity and WebView
   * 
   * @param activity
   *          Activity we are running in
   * @param webView
   *          WebView that we are interfaceing with
   */
  public JavaScriptInterface(Activity activity, WebView webView) {
    this.activity = activity;
    this.integrator = new IntentIntegrator(this.activity);
    this.webView = webView;
  }

  /**
   * Called to check the signature of the specified message and Signature. This uses the default key located in the Assets folder in
   * the file jointkey.json
   * 
   * @param msg
   *          String containing the message content to be checked
   * @param signature
   *          String in Base64 format of the signature we are actually checking
   */
  @JavascriptInterface
  public void checkSignature(String msg, String signature) {
    try {
      // Read the public key
      BufferedReader br = null;
      try {
        br = new BufferedReader(new InputStreamReader(this.activity.getAssets().open("jointkey.json")));
        StringBuffer sb = new StringBuffer();
        String line = null;
        while ((line = br.readLine()) != null) {
          sb.append(line);
        }
        // Create JSONObject
        JSONObject jointKey = new JSONObject(sb.toString());
        // Get the actual Key Object
        BLSPublicKey blsPubKey = new BLSPublicKey(jointKey.getJSONObject("WBB").getJSONObject("pubKeyEntry"));

        // Construct a new TVSSiganture to use for verification
        TVSSignature tvsSig = new TVSSignature(blsPubKey, false);
        // Update it with the message
        tvsSig.update(msg);

        // Prepare a progress dialog to show whilst we do the actual verification
        ProgressDialog pd = new ProgressDialog(this.activity);
        pd.setMessage("Checking Signature...\n(This may take several minutes)");
        pd.setCancelable(false);
        pd.setIndeterminate(true);

        // Create the AsyncTask that will perform the actual signature check
        SignatureChecker sigChecker = new SignatureChecker(pd, tvsSig, IOUtils.decodeData(EncodingType.BASE64, signature),
            this.webView);

        // Execute the signature check
        sigChecker.execute();

      }
      finally {
        // Close the reader
        if (br != null) {
          br.close();
        }
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    catch (JSONException e) {
      e.printStackTrace();
    }
    catch (TVSSignatureException e) {
      e.printStackTrace();
    }

  }

  /**
   * Method to check a signature with an external key. This has not yet been implemented
   * 
   * @param keyJson
   * @param msg
   * @param signature
   * @return
   */
  @JavascriptInterface
  public boolean checkSignatureWithKey(String keyJson, String msg, String signature) {
    System.err.println("Check signature with specific key not yet implemented");
    return false;
  }

  /**
   * Call to scan a barcode using the BarcodeScanner intent. This will only look for QRCodes
   */
  @JavascriptInterface
  public void scanBarcode() {
    this.integrator.initiateScan(IntentIntegrator.QR_CODE_TYPES);

  }
}
