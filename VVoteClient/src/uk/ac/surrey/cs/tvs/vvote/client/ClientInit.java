/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.client;

import uk.ac.surrey.cs.tvs.client.Client;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * ClientInit is the registered BroadcastReceiver for receiving the intents from adb. It hands off the actual processing to a
 * BroadcastWorker. This is required to prevent an android error by performing too much work on the calling thread.
 * 
 * @author Chris Culnane
 * 
 */
public class ClientInit extends BroadcastReceiver {

  /**
   * Static intent value to get the status
   */
  public static final String ACTION_GET_STATUS          = "uk.ac.surrey.cs.tvs.clientui.getstatus";

  /**
   * Static intent value to create the signing keys
   */
  public static final String ACTION_CREATE_SIGNING_KEYS = "uk.ac.surrey.cs.tvs.clientui.createsigningkeys";

  /**
   * Static intent value to create the crypto keys - if applicable
   */
  public static final String ACTION_CREATE_CRYPTO_KEYS  = "uk.ac.surrey.cs.tvs.clientui.createcryptokeys";

  /**
   * Static intent to import the CSR response from the CA
   */
  public static final String ACTION_IMPORT_CSR          = "uk.ac.surrey.cs.tvs.clientui.importcsr";

  /**
   * Static intent for generate the ballots - if applicable
   */
  public static final String ACTION_GEN_BALLOTS         = "uk.ac.surrey.cs.tvs.clientui.genballots";

  /**
   * Reference to the Client object the operations are to be performed on
   */
  private Client             client;

  /**
   * Creates a new ClientInit BroadCastReceiver which will performed received actions on the specified Client object
   * 
   * @param client
   *          Client object to perform the actions on
   */
  public ClientInit(Client client) {
    this.client = client;
  }

  /**
   * Fired when an intent is received of the appropriate type. Constructs a new BroadcastWorker and runs it in a daemon thread
   */
  @Override
  public void onReceive(Context ctx, Intent intent) {
    BroadcastWorker bw = new BroadcastWorker(this.client, intent);
    Thread t = new Thread(bw);
    t.setDaemon(true);
    t.start();

  }

}
