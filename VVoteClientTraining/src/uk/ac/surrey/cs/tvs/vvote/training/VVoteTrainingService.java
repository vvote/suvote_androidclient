/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.training;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vvote.json.JSONException;
import org.vvote.json.JSONObject;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyException;
import uk.ac.surrey.cs.tvs.client.ClientType;
import uk.ac.surrey.cs.tvs.client.TrainingClient;
import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.comms.http.VECWebSocket;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.vvote.client.AndroidConstants;
import uk.ac.surrey.cs.tvs.vvote.client.BarcodeServlet;
import uk.ac.surrey.cs.tvs.vvote.client.EpsonPrinter;
import uk.ac.surrey.cs.tvs.vvote.client.EpsonPrinter.ConnectionType;
import uk.ac.surrey.cs.tvs.vvote.client.PrintServlet;
import uk.ac.surrey.cs.tvs.vvote.client.VVoteServiceInterface;
import uk.ac.surrey.cs.tvs.vvote.training.AndroidConstantsTraining.Paths;
import uk.ac.surrey.cs.tvs.vvote.webcam.WebCamFeed;
import uk.ac.surrey.cs.tvs.web.ProxyServer;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.widget.Toast;

/**
 * VVoteTrainingService provides an android service for running all the vVote client functions, from print servlets through to proxy
 * servlets for communicating with the MBB.
 * 
 * @author Chris Culnane
 * 
 */
public class VVoteTrainingService extends Service implements VVoteServiceInterface {

  /**
   * Logger
   */
  private static final Logger logger                        = LoggerFactory.getLogger(VVoteTrainingService.class);

  /**
   * EpsonPrinter we will use for printing
   */
  private EpsonPrinter        epsonPrinter;

  /**
   * Underlying webserver we are running to host servlets
   */
  private ProxyServer         proxyServer;

  /**
   * Underlying webserver for the EVM - TODO eventually we will only need one proxy, however, for integration it is useful to have
   * both proxies running on the same device
   */
  private ProxyServer         proxyServerEVM;

  /**
   * BarcodeServlet used for scanning QRCodes
   */
  private BarcodeServlet      barcodeServlet;

  /**
   * variables to monitor whether the server is running
   */
  private boolean             serverRunning                 = false;

  /**
   * Config file used to configure this instance
   */
  private ConfigFile          conf;

  /**
   * WebSocket to listen for WebCamFeed requests
   */
  private VECWebSocket        webcamSocket;
  /**
   * WebCamFeed to use as a listener on the WebSocket
   */
  private WebCamFeed          webCamFeed;

  /**
   * Static variable for Request string in intent
   */
  public static final String  REQUEST_STRING                = "vVoteServiceRequest";

  /**
   * Static variable for Request string in intent
   */
  public static final String  RESPONSE_STRING               = "vVoteServiceRequestType";

  /**
   * Response field value 
   */
  public static final String  RESPONSE_MESSAGE              = "vVoteServiceRequestMsg";

  /**
   * Requests last download property
   */
  public static final String  REQUEST_LAST_DOWNLOAD         = "lastDownload";
  /**
   * Requests a general status update
   */
  public static final String  REQUEST_STATUS                = "status";

  /**
   * Requests the download status
   */
  public static final String  REQUEST_DOWNLOAD_STATUS       = "dlStatus";

  /**
   * Requests a force download attempt
   */
  public static final String  REQUEST_DOWNLOAD              = "dlForce";

  /**
   * Intent response id
   */
  public static final String  PROCESS_RESPONSE              = "uk.ac.surrey.cs.tvs.vvote.training.PROCESS_RESPONSE";
  /**
   * indicates whether the webcam is connected
   */
  public static final String  RESP_WEBCAM_CONNECTED         = "webcamConnected";

  /**
   * indicates if the printer is ready
   */
  public static final String  RESP_PRINTER_CONNECTED        = "printerReady";

  /**
   * Indicates if the server is running
   */
  public static final String  RESP_SERVER_RUNNING           = "serverRunning";

  /**
   * indicates if the ClientUI (initialisation) server is running
   */
  public static final String  RESP_CLIENTUI_RUNNING         = "clientUI";

  /**
   * indicates if the POD proxy is running
   */
  public static final String  RESP_PROXY_SERVER_RUNNING     = "proxyServer";

  /**
   * indicates if the EVM proxy is running
   */
  public static final String  RESP_PROXY_SERVER_EVM_RUNNING = "proxyServerEVM";

  /**
   * Default constructor to build VVoteTrainingService, actual startup work is done in onCreate
   */
  public VVoteTrainingService() {
  }

  /**
   * Gets the ConfigFile for the service
   * 
   * @return ConfigFile related to this service
   */
  @Override
  public ConfigFile getConfig() {
    return this.conf;
  }

  /**
   * Gets the current instance of the EpsonPrinter object
   * 
   * @return EpsonPrinter instance currently initialised
   */
  @Override
  public EpsonPrinter getEpsonPrinter() {
    return this.epsonPrinter;
  }

  @Override
  public Service getService() {
    return this;
  }

  /**
   * This isn't supported, throws an exception if called
   */
  @Override
  public IBinder onBind(Intent intent) {
    logger.error("Received an onBind request - this isn't supported, will throw exception");
    throw new UnsupportedOperationException("Not yet implemented");
  }

  /**
   * Called when the service is created - this is where we initialise everything. However, we do not start the server until onStart
   * is called
   */
  @Override
  public void onCreate() {
    try {
      // Load GMP and PBC native libraries
      AndroidConstants.loadLibrary();
      Notification.Builder builder = new Notification.Builder(this.getApplicationContext());
      Intent i = new Intent(this, VVoteTrainingManager.class);

      i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

      PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);
      builder.setContentIntent(pi)

      .setSmallIcon(R.drawable.ic_notify).setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_launcher))
          .setTicker("TrainingMode Is Running").setWhen(System.currentTimeMillis()).setContentTitle("TrainingMode Is Running")
          .setContentText("TrainingMode Is Running");
      Notification n = builder.build();
      n.flags |= Notification.FLAG_NO_CLEAR;

      this.startForeground(530, n);
      // Get a config file
      this.conf = new ConfigFile(Paths.CONFIG_FILE);
      logger.info("Loaded config");
      logger.info("Creating a new service instance");
      Toast.makeText(this, "vVote Printing Service has Initialised", Toast.LENGTH_LONG).show();
      logger.info("Creating EpsonPrinter object");
      this.epsonPrinter = new EpsonPrinter(ConnectionType.USB, this.getApplicationContext());

      TrainingClient client = new TrainingClient(Paths.CLIENT_CONFIG);

      logger.info("Building Servlets List");
      logger.info("Adding PrintMe Servlet");
      PrintServlet ps = new PrintServlet(this);
      this.barcodeServlet = new BarcodeServlet(this);

      if (client.getType() == ClientType.EVM) {
        this.proxyServerEVM = new ProxyServer(Paths.CLIENT_CONFIG, Paths.DISTRICT_CONFIG);
        this.proxyServerEVM.addServlet("printMe", ps);
        this.proxyServerEVM.addServlet("getBarcode", this.barcodeServlet);

      }
      else if (client.getType() == ClientType.VPS) {
        this.proxyServer = new ProxyServer(Paths.CLIENT_CONFIG, Paths.DISTRICT_CONFIG);
        this.proxyServer.addServlet("printMe", ps);
        this.proxyServer.addServlet("getBarcode", this.barcodeServlet);

      }
      this.webCamFeed = new WebCamFeed(this);
    }
    catch (JSONIOException e) {
      logger.error("Exception whilst loading servlet config", e);
    }
    catch (IOException e) {
      logger.error("Exception whilst loading servlet config", e);
    }
    catch (PeerSSLInitException e) {
      logger.error("Exception whilst loading servlet config", e);
    }
    catch (MaxTimeoutExceeded e) {
      logger.error("Exception whilst loading servlet config", e);
    }
    catch (CryptoIOException e) {
      logger.error("Exception whilst loading servlet config", e);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst loading servlet config", e);
    }

  }

  /**
   * Called when the service is to be destroyed. We shutdown the webserver and disconnect from the printer
   */
  @Override
  public void onDestroy() {
    logger.info("Destroying service");

    logger.info("Stopping internal webserver");
    if (this.proxyServer != null && this.proxyServer.isRunning()) {

      this.proxyServer.stopServer();

    }
    this.proxyServer = null;

    if (this.proxyServerEVM != null && this.proxyServerEVM.isRunning()) {
      this.proxyServerEVM.stopServer();

    }
    this.proxyServerEVM = null;
    if (this.webCamFeed != null) {
      this.webCamFeed.stop();
    }
    try {
      if (this.webcamSocket != null) {
        this.webcamSocket.stop();
      }
    }
    catch (IOException e) {
      logger.error("Exception whilst closing WebSocket", e);
    }
    catch (InterruptedException e) {
      logger.error("Exception whilst closing WebSocket", e);
    }
    try {
      logger.info("Closing printer connection");
      if (this.epsonPrinter != null) {
        this.epsonPrinter.close();
      }
    }
    catch (IOException e) {
      logger.error("Exception whilst destroying service and closing connection to printer");
    }
    Toast.makeText(this, "vVote Printing Service has Stopped", Toast.LENGTH_LONG).show();
    this.serverRunning = false;
  }

  /**
   * Called when the service is to be started. Starts the previously configured webservice
   */
  @Override
  public void onStart(Intent intent, int startid) {

    logger.info("Starting service called: ServerRunning {}", this.serverRunning);

    if (!this.serverRunning) {
      Toast.makeText(this, "vVote Printing Service has Started", Toast.LENGTH_LONG).show();
      logger.info("Starting server");
      (new Thread() {

        @Override
        public void run() {
          try {
            logger.info("Starting server");
            if (VVoteTrainingService.this.proxyServer != null) {
              VVoteTrainingService.this.proxyServer.startServer(8090,
                  new File(VVoteTrainingService.this.conf.getStringParameter(AndroidConstants.Config.SERVER_ROOT)));
            }
            if (VVoteTrainingService.this.proxyServerEVM != null) {
              VVoteTrainingService.this.proxyServerEVM.startServer(8060,
                  new File(VVoteTrainingService.this.conf.getStringParameter(AndroidConstants.Config.SERVER_ROOT)));
            }
            logger.info("Server Started");
            VVoteTrainingService.this.webcamSocket = new VECWebSocket(new InetSocketAddress("localhost", 9099));
            VVoteTrainingService.this.webcamSocket.addWebSocketListener(VVoteTrainingService.this.webCamFeed);
            VVoteTrainingService.this.webcamSocket.start();
            VVoteTrainingService.this.sendStatus();
          }
          catch (IOException e) {
            logger.error("Exception whilst starting server", e);
          }
          catch (ProxyException e) {
            logger.error("Exception whilst starting server", e);
          }

        }
      }).start();

      this.serverRunning = true;
    }

  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    this.onStart(intent, startId);
    logger.info("Received start id {}:{}", startId, intent);
    String request = null;
    if (intent != null && intent.getExtras() != null) {
      request = intent.getExtras().getString(REQUEST_STRING, null);
    }
    if (request != null) {
      if (request.equalsIgnoreCase(REQUEST_STATUS)) {
        this.sendStatus();
      }
    }

    // We want this service to continue running until it is explicitly
    // stopped, so return sticky.
    return START_STICKY;

  }

  /**
   * Forces a reconnect of the printer in case it has gone offline
   * 
   * @return
   */
  @Override
  public EpsonPrinter reconnectPrinter() {
    logger.info("Reconnecting to printer");
    if (this.epsonPrinter != null) {
      try {
        this.epsonPrinter.close();
      }
      catch (IOException e) {
        logger.error("Exception when closing old printer");
      }
    }
    this.epsonPrinter = new EpsonPrinter(ConnectionType.USB, this.getApplicationContext());
    return this.epsonPrinter;
  }

  @Override
  public void sendStatus() {
    try {
      JSONObject resp = new JSONObject();
      resp.put(RESP_WEBCAM_CONNECTED, this.barcodeServlet.cameraDetected());
      resp.put(RESP_PRINTER_CONNECTED, this.epsonPrinter.isPrinterReady());
      resp.put(RESP_SERVER_RUNNING, this.serverRunning);

      resp.put(RESP_CLIENTUI_RUNNING, false);
      if (this.proxyServer != null) {
        resp.put(RESP_PROXY_SERVER_RUNNING, this.proxyServer.isRunning());
      }
      else {
        resp.put(RESP_PROXY_SERVER_RUNNING, false);
      }
      if (this.proxyServerEVM != null) {
        resp.put(RESP_PROXY_SERVER_EVM_RUNNING, this.proxyServerEVM.isRunning());
      }
      else {
        resp.put(RESP_PROXY_SERVER_EVM_RUNNING, false);
      }
      Intent broadcastIntent = new Intent();
      broadcastIntent.setAction(PROCESS_RESPONSE);
      broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
      broadcastIntent.putExtra(RESPONSE_STRING, REQUEST_STATUS);
      broadcastIntent.putExtra(RESPONSE_MESSAGE, resp.toString());
      this.sendBroadcast(broadcastIntent);
    }
    catch (JSONException e) {
      logger.error("Exception creating Intent response", e);
    }
  }

  /**
   * Sets a new EpsonPrinter instance in case a different method has modified the connection. The preferred option is to use
   * reconnectPrinter
   * 
   * @param updatedPrinter
   *          EpsonPrinter new instance of the EpsonPrinter to now use
   */
  @Override
  public void setEpsonPrinter(EpsonPrinter updatedPrinter) {
    if (this.epsonPrinter != null) {
      try {
        this.epsonPrinter.close();
      }
      catch (IOException e) {
        logger.error("Exception when closing old printer");
      }
    }
    this.epsonPrinter = updatedPrinter;
  }
}
